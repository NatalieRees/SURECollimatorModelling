Natalie Rees SURE summer internship, University of Leicester

Project on 'Modelling the performance of high-energy collimators for medical imaging and high-energy physics'

Completed July - August 2019

Note: project completed before knowledge of git so has been added to GitLab at a later date
