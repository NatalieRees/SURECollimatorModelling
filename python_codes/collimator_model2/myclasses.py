#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 12:28:23 2019

@author: Natalie
"""


class IncidentPhotons():
    def _init_(self):
        self.pos = Cartesian()
        self.angle = Polar()
        self.en = []
        self.frame = []
        self.s =[]
        self.nphot = []
        
class Detector():
    def _init_(self):
        self.zdep = []
        self.x = []
        self.y = []
        
class Element():
    def _init_(self):
        self.denisty = []
        self.atten = []
        self.Z = []
        
class Cartesian():
    def _init_(self):
        self.x = []
        self.y = []
        self.z = []

#x = r sin(theta)cos(phi)
#y = r sin(theta)sin(phi)
#z = r cos(theta)
class Polar():
    def _init_(self):
        self.theta = []
        self.phi = []