#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 09:13:23 2019

@author: Natalie
"""


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os

import myclasses as myclass
import myfunctions_square as myfunc

#define home directory 
homedir = '/Users/Natalie/Documents/SURE_2019_student/python_codes/collimator_model2'

#Define simulation inputs

#Input parameters for source
CRATE = 1
NFRAME = 10000
NPHOT = CRATE*NFRAME
SOURCE = '10000' #line energy in keV

#Create collimator 
COL = myclass.Detector()
COL.zdep = 25.0 
COL.x = 20
COL.y = 20
COL.hole = 10 

hole_left = (COL.x - COL.hole)/2
hole_right = (COL.hole + COL.x)/2
hole = np.array([hole_left,hole_right])

#Object and input parameters for material of collimator
W = myclass.Element()
W.atten = pd.read_csv('W_absorptioncoe_all.csv')
W.Z = 74
W.density = 19.3 #g/cm^3
W.u = 183.84

#Object to store all incident photon information
iphot = myclass.IncidentPhotons()
iphot.pos = myclass.Cartesian()

#Assign energy to each photon
iphot.en = myfunc.radio_model(SOURCE,NPHOT)

#Assign random position in x and y to each photon
iphot.pos.x = np.random.uniform(0,COL.x,NPHOT)
iphot.pos.y = np.random.uniform(0,COL.y,NPHOT)

iphot.pos.z = myfunc.depletion_depth(iphot.en,W.atten,W.density)

direct = myfunc.pass_thru_holes(iphot,hole)
indirect = np.setdiff1d(myfunc.attenuated(iphot,COL.zdep),direct)

f1 = plt.figure()
ax = f1.add_subplot(111)
ax.plot(iphot.pos.x[direct[0]],iphot.pos.y[direct[0]],'o',markersize = 1)
ax.plot(iphot.pos.x[indirect],iphot.pos.y[indirect],'o',markersize = 1)
ax.set_xlim(0,COL.x)
ax.set_ylim(0,COL.y)


