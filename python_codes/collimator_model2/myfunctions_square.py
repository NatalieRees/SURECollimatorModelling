#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 12:29:22 2019

@author: Natalie
"""

import numpy as np
from scipy.interpolate import interp1d

def radio_model(source,nphot):
    #assign photon energy for a single emission line
    line = float(source)
    ephot = np.tile(line,nphot)
    return ephot

def depletion_depth(ephot,df,density):
    """Return the depletion depth (mm) of the photon as a function of its energy.
    Use log-log interpolation for best result."""
    
    f = interp1d(np.log10(df['keV']),np.log10(df['Tot(cm2/g)']*density))
    u = f(np.log10(ephot))
    u = 10**u
    P_z = np.random.random(len(ephot))
    z = (-1/u)*np.log(P_z)
    return z*10

def pass_thru_holes(iphot,hole):
    successful = []
    successful.append(np.where( (hole[0]<iphot.pos.x) & (hole[1]>iphot.pos.x)
    & (hole[0]<iphot.pos.y) & (hole[1]>iphot.pos.y))[0])
    return successful

def attenuated(iphot,zdep):
    not_attenuated = np.where(iphot.pos.z > zdep)
    return not_attenuated
