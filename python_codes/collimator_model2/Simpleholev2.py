#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 26 11:20:08 2019

@author: Natalie
"""


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import time

import myclasses as myclass
import myfunctions_circle as myfunc

#define home directory 
homedir = '/Users/Natalie/Documents/SURE_2019_student/python_codes/collimator_model2'

start_time = time.time()

#Define simulation inputs

#Input parameters for source
CRATE = 1
NFRAME = 1000000
NPHOT = CRATE*NFRAME
SOURCE = '122' #line energy in keV

#Create collimator 
COL = myclass.Detector()
COL.zdep = 1.0 #mm
COL.x = 20.0 #mm
COL.y = 20.0 #mm
COL.hole = 2.0 #radius of collimator hole (mm)
COL.h = 376.3 #Distance between collimator and source (mm)

#For flood image
FLOOD = myclass.Detector()
FLOOD.zdep = 0.0 #mm
FLOOD.x = 20.0 #mm
FLOOD.y = 20.0 #mm
FLOOD.hole = 0 #radius of collimator hole (mm)
FLOOD.h = COL.h + COL.zdep

#Create detector
DET = myclass.Detector()
DET.x = COL.x #mm
DET.y = COL.y #mm
DET.pixsize = 0.25 #mm
DET.gap = 20 #Gap between collimator and detector (mm)

#Object and input parameters for material of collimator

#Tungsten
W = myclass.Element()
W.atten = pd.read_csv('W_absorptioncoe_all.csv')
W.Z = 74
W.density = 19.3 #g/cm^3

#Lead
Pb = myclass.Element()
Pb.atten = pd.read_csv('Pb_absorptioncoe_all.csv')
Pb.Z = 82
Pb.density = 11.34 #g/cm^3

atten = W.atten
density = W.density

iphot = myclass.IncidentPhotons()
iphot.pos = myclass.Cartesian()
iphot.nphot = NPHOT

#Assign energy to each photon
iphot.en = myfunc.radio_model(SOURCE,iphot.nphot)

#Assign random angles, assuming isotropy
proportion = myfunc.assign_angles(iphot,COL,DET)

#Find positions of photons at bottom of collimator
myfunc.cartesian_pos(iphot,COL.h+COL.zdep)
    
#Case 1: Find photons that pass directly through the hole
case1 = myfunc.pass_thru_radius(iphot,COL.hole)
iphot.s = np.array([0.0]*iphot.nphot)

#Case 2: Photons travel partially through hole 
R_crit = (COL.h+COL.zdep)*COL.hole/COL.h
case2 = np.setdiff1d(myfunc.pass_thru_radius(iphot,R_crit),case1)
R = np.sqrt(iphot.pos.x**2+iphot.pos.y**2)
iphot.s[case2] = (R[case2]-COL.hole)/np.sin(iphot.angle.theta[case2])

#Case 3: Photons travel fully through collimator
case3 = np.setdiff1d(np.arange(iphot.nphot),myfunc.pass_thru_radius(iphot,R_crit))
iphot.s[case3] = COL.zdep/np.cos(iphot.angle.theta[case3])
    
#Project positions onto detector 
myfunc.cartesian_pos(iphot,COL.h+COL.zdep+DET.gap)
    
#Finds photons that land outside the detector
inside = np.where((iphot.pos.x<DET.x/2)&(iphot.pos.x>-DET.x/2)&
                      (iphot.pos.y<DET.y/2)&(iphot.pos.y>-DET.y/2))
outside = np.setdiff1d(np.arange(iphot.nphot),inside)

detected = np.setdiff1d(np.where(myfunc.depletion_depth(iphot.en,atten,density)>iphot.s),outside)

print("Flood:")
print("Counts detected = ",len(inside[0]))
print('Sensitivity = ', len(inside[0])/iphot.nphot*proportion)

print("\nWith collimator")
print("Counts detected = ",len(detected))
print('Sensitivity = ', len(detected)/iphot.nphot*proportion)

print('\nCollimator efficiency =',len(detected)/len(inside[0])*100,'%')

#Plots positions of individual photons
f1 = plt.figure()
ax = f1.add_subplot(111)
ax.plot(iphot.pos.x[detected],iphot.pos.y[detected],'o',markersize=0.1)
ax.set_xlim(-COL.x/2,COL.x/2)
ax.set_ylim(-COL.y/2,COL.y/2)
ax.set_xlabel('$x$ (mm)')
ax.set_ylabel('$y$ (mm)')
hole = plt.Circle((0,0),COL.hole,color="black",fill=False)
ax.add_patch(hole)
ax.set_title('h = '+str(COL.h))

#Plots histogram
x = iphot.pos.x[detected]
y = iphot.pos.y[detected]
bins = np.arange(-DET.x/2,DET.x/2+DET.pixsize,DET.pixsize)
f2 = plt.figure()
ax = f2.add_subplot(111)
im = ax.hist2d(x,y,bins=(bins,bins),cmap='coolwarm')
cb  = plt.colorbar(im[3],shrink=0.9,spacing='proportional')
cb.set_label('counts',rotation=270,labelpad=15)
hole = plt.Circle((0,0),COL.hole,color="black",fill=False)
ax.add_patch(hole)
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')
ax.set_xlim(-DET.x/2,DET.x/2)
ax.set_ylim(-DET.y/2,DET.y/2)
ax.set_title('h = '+str(COL.h))

print('\nRun time = ',time.time()-start_time,'s')