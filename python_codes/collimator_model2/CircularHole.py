#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 09:13:23 2019

@author: Natalie
"""


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os

import myclasses as myclass
import myfunctions_circle as myfunc

#define home directory 
homedir = '/Users/Natalie/Documents/SURE_2019_student/python_codes/collimator_model2'

#Define simulation inputs

#Input parameters for source
CRATE = 1
NFRAME = 10000
NPHOT = CRATE*NFRAME
SOURCE = '1000' #line energy in keV

#Create collimator 
COL = myclass.Detector()
COL.zdep = 20.0 #mm
COL.x = 20 #mm
COL.y = 20 #mm
COL.hole = 5 #radius of hole (mm)

#Create detector
DET = myclass.Detector()
DET.x = 20 #mm
DET.y = 20 #mm
DET.pixsize = 0.5 #mm

#Object and input parameters for material of collimator
W = myclass.Element()
W.atten = pd.read_csv('W_absorptioncoe_all.csv')
W.Z = 74
W.density = 19.3 #g/cm^3
W.u = 183.84

#Object to store all incident photon information
iphot = myclass.IncidentPhotons()
iphot.pos = myclass.Cartesian()

#Assign energy to each photon
iphot.en = myfunc.radio_model(SOURCE,NPHOT)

#Assign random position in x and y to each photon, hole is centered at the origin
iphot.pos.x = np.random.uniform(-COL.x/2,COL.x/2,NPHOT)
iphot.pos.y = np.random.uniform(-COL.y/2,COL.y/2,NPHOT)

iphot.pos.z = myfunc.depletion_depth(iphot.en,W.atten,W.density)

direct = myfunc.pass_thru_radius(iphot,COL.hole)
indirect = np.setdiff1d(myfunc.attenuated(iphot,COL.zdep),direct)

#Plots positions of individual photons
f1 = plt.figure(figsize = (10,10))
ax = f1.add_subplot(111)
ax.plot(iphot.pos.x[direct],iphot.pos.y[direct],'o',markersize = 1)
ax.plot(iphot.pos.x[indirect],iphot.pos.y[indirect],'o',markersize = 1)
ax.set_xlim(-COL.x/2,COL.x/2)
ax.set_ylim(-COL.y/2,COL.y/2)
ax.set_xlabel('$x$ (mm)')
ax.set_ylabel('$y$ (mm)')
#Shape of collimator hole
hole = plt.Circle((0,0),COL.hole,color="black",fill=False)
ax.add_patch(hole)

#Plots histogram
x = iphot.pos.x[np.append(direct,indirect)]
y = iphot.pos.y[np.append(direct,indirect)]
bins = np.arange(-DET.x/2,DET.x/2+DET.pixsize,DET.pixsize)

f2 = plt.figure(figsize = (10,10))
ax = f2.add_subplot(111)
im = ax.hist2d(x,y,bins=(bins,bins))
cb  = plt.colorbar(im[3],shrink=0.9)
cb.set_label('counts',rotation=270,labelpad=15)
hole = plt.Circle((0,0),COL.hole,color="black",fill=False)
ax.add_patch(hole)
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')
ax.set_xlim(-DET.x/2,DET.x/2)
ax.set_ylim(-DET.y/2,DET.y/2)


