#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 26 11:20:08 2019

@author: Natalie
"""


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import myclasses as myclass
import myfunctions_circle as myfunc

#define home directory 
homedir = '/Users/Natalie/Documents/SURE_2019_student/python_codes/collimator_model2'

#Define simulation inputs

#Input parameters for source
CRATE = 1
NFRAME = 1000000
NPHOT = CRATE*NFRAME
SOURCE = '122' #line energy in keV

#Create collimator 
COL = myclass.Detector()
COL.zdep = 1.0 #mm
COL.x = 20.0 #mm
COL.y = 20.0 #mm
COL.hole = 2.0 #radius of collimator hole (mm)

#Distance between collimator and source
h = 376.3 #mm

#For flood image
FLOOD = myclass.Detector()
FLOOD.zdep = 0.0 #mm
FLOOD.x = 20.0 #mm
FLOOD.y = 20.0 #mm
FLOOD.hole = 0 #radius of collimator hole (mm)

h_flood = h + COL.zdep

#Create detector
DET = myclass.Detector()
DET.x = COL.x #mm
DET.y = COL.y #mm
DET.pixsize = 0.25 #mm
DET.gap = 20 #Gap between collimator and detector (mm)

#Object and input parameters for material of collimator

#Tungsten
W = myclass.Element()
W.atten = pd.read_csv('W_absorptioncoe_all.csv')
W.Z = 74
W.density = 19.3 #g/cm^3

#Lead
Pb = myclass.Element()
Pb.atten = pd.read_csv('Pb_absorptioncoe_all.csv')
Pb.Z = 82
Pb.density = 11.34 #g/cm^3

flood_detected = myfunc.run(FLOOD,DET,h_flood,SOURCE,NPHOT,W.atten,W.density)

coll_detected = myfunc.run(COL,DET,h_flood,SOURCE,NPHOT,W.atten,W.density)

print('Collimator efficiency =',len(coll_detected)/len(flood_detected)*100,'%')
