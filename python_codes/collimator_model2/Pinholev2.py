#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 26 11:39:11 2019

@author: Natalie
"""


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
import time

import myclasses as myclass
import myfunctions_circle as myfunc

#define home directory 
homedir = '/Users/Natalie/Documents/SURE_2019_student/python_codes/collimator_model2/'

start_time = time.time()

#Define simulation inputs

#Input parameters for source
CRATE = 1
NFRAME = 10000000
NPHOT = CRATE*NFRAME
SOURCE = '122' #line energy in keV

#Create collimator 
COL = myclass.Detector()
COL.x = 20.0 #mm
COL.y = 20.0 #mm
COL.smallhole = 1 #radius of small collimator hole (mm)
COL.bighole = 1.85 #radius of big collimator hole (mm)
COL.alpha = 31*np.pi/360 #Angle at focus of pinhole = 2*alpha
COL.zdep = 2*(COL.bighole-COL.smallhole)/np.tan(COL.alpha) #mm
COL.h = 53.7 #Distance between collimator and source (mm)

#Create detector
DET = myclass.Detector()
DET.x = COL.x #mm
DET.y = COL.y #mm
DET.pixsize = 0.25 #mm
DET.gap = 20 #Distance between collimator and detector (mm)

#Distance between source and detector 
#L = 50.0 #mm

#Calculates optimum distances between source, collimator and detector to fill detector 
#h_opt = (COL.bighole+COL.smallhole)*L/COL.x - 0.5*COL.zdep
#gap_opt = L-h_opt-COL.zdep

#Distance below which program needs altering
h_crit = COL.bighole/np.tan(COL.alpha) - COL.zdep #mm
if COL.h<h_crit:
    print('Source too close to collimator')
    
#Object and input parameters for material of collimator
#Tungsten
W = myclass.Element()
W.atten = pd.read_csv('W_absorptioncoe_all.csv')
W.Z = 74
W.density = 19.3 #g/cm^3

#Lead
Pb = myclass.Element()
Pb.atten = pd.read_csv('Pb_absorptioncoe_all.csv')
Pb.Z = 82
Pb.density = 11.34 #g/cm^3

atten = W.atten
density = W.density

iphot = myclass.IncidentPhotons()
iphot.pos = myclass.Cartesian()
iphot.nphot = NPHOT

#Assign energy to each photon
iphot.en = myfunc.radio_model(SOURCE,iphot.nphot)

#Assign random angles, assuming isotropy
proportion = myfunc.assign_angles(iphot,COL,DET)

#Find positions of photons at bottom of collimator
myfunc.cartesian_pos(iphot,COL.h+COL.zdep)

#Case 1: Find photons that pass directly through the hole
R_crit1 = (COL.h+COL.zdep)*COL.smallhole/(COL.h+COL.zdep/2)
case1 = myfunc.pass_thru_radius(iphot,R_crit1)
iphot.s = np.array([0.0]*iphot.nphot)

#Case 2: Photons travel partially through collimator, B>R>R1
case2 = np.setdiff1d(myfunc.pass_thru_radius(iphot,COL.bighole),case1)
G = (COL.zdep/2+COL.h)*np.tan(iphot.angle.theta[case2])-COL.smallhole
b1 = np.cos(COL.alpha)/np.sin(iphot.angle.theta[case2]+COL.alpha)*G
b2 = np.cos(COL.alpha)/np.sin(-iphot.angle.theta[case2]+COL.alpha)*G
iphot.s[case2] = b1+b2 

#Case 3: Photons travel partially through collimator, R2>R>B
R_crit2 = (COL.h+COL.zdep)*COL.bighole/COL.h
case3 = np.setdiff1d(myfunc.pass_thru_radius(iphot,R_crit2),myfunc.pass_thru_radius(iphot,COL.bighole))
G = (COL.zdep/2+COL.h)*np.tan(iphot.angle.theta[case3])-COL.smallhole
b = np.cos(COL.alpha)/np.sin(iphot.angle.theta[case3]+COL.alpha)*G
iphot.s[case3] = COL.zdep/(2*np.cos(iphot.angle.theta[case3])) + b

#Case4: Photons travel fully through collimator
case4 = np.setdiff1d(np.arange(iphot.nphot),myfunc.pass_thru_radius(iphot,R_crit2))
iphot.s[case4] = COL.zdep/np.cos(iphot.angle.theta[case4])
    
#Project positions onto detector 
myfunc.cartesian_pos(iphot,COL.h+COL.zdep+DET.gap)
    
#Finds photons that land outside the detector
inside = np.where((iphot.pos.x<DET.x/2)&(iphot.pos.x>-DET.x/2)&
                      (iphot.pos.y<DET.y/2)&(iphot.pos.y>-DET.y/2))
outside = np.setdiff1d(np.arange(iphot.nphot),inside)

detected = np.setdiff1d(np.where(myfunc.depletion_depth(iphot.en,atten,density)>iphot.s),outside)
    
print("Flood:")
print("Counts detected = ",len(inside[0]))
print('Sensitivity = ', len(inside[0])/iphot.nphot*proportion)


print("\nWith collimator")
print("Counts detected = ",len(detected))
print('Sensitivity = ', len(detected)/iphot.nphot*proportion)

print('\nCollimator efficiency =',len(detected)/len(inside[0])*100,'%')

#Directory to save results
workdir = 'Pinhole_' + SOURCE + 'keV'
try:
    os.mkdir(homedir + workdir)
except:
    print('directory already exists...')

os.chdir(homedir + workdir)

#Plots positions of individual photons
f1 = plt.figure()
ax = f1.add_subplot(111)
ax.plot(iphot.pos.x[detected],iphot.pos.y[detected],'o',markersize=0.1)
ax.set_xlim(-COL.x/2,COL.x/2)
ax.set_ylim(-COL.y/2,COL.y/2)
ax.set_xlabel('$x$ (mm)')
ax.set_ylabel('$y$ (mm)')
smallhole = plt.Circle((0,0),COL.smallhole,color="black",fill=False)
ax.add_patch(smallhole)
bighole = plt.Circle((0,0),COL.bighole,color="black",fill=False)
ax.add_patch(bighole)
ax.set_title('h = '+str(COL.h)+'mm, gap = '+ str(DET.gap)[0:4] + 'mm, ' + str(SOURCE) +'keV')
plt.savefig('h = '+str(COL.h)+'mm, gap = ' + str(DET.gap)[0:4] + 'mm.png',dpi=500)

#Plots histogram
x = iphot.pos.x[detected]
y = iphot.pos.y[detected]
bins = np.arange(-DET.x/2,DET.x/2+DET.pixsize,DET.pixsize)
f2 = plt.figure()
ax = f2.add_subplot(111)
im = ax.hist2d(x,y,bins=(bins,bins),cmap='coolwarm')
cb  = plt.colorbar(im[3],shrink=0.9,spacing='proportional')
cb.set_label('counts',rotation=270,labelpad=15)
smallhole = plt.Circle((0,0),COL.smallhole,color="black",fill=False)
ax.add_patch(smallhole)
bighole = plt.Circle((0,0),COL.bighole,color="black",fill=False)
ax.add_patch(bighole)
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')
ax.set_xlim(-DET.x/2,DET.x/2)
ax.set_ylim(-DET.y/2,DET.y/2)
ax.set_title('h = '+str(COL.h)+'mm, gap = '+ str(DET.gap)[0:4] + 'mm, ' + str(SOURCE) +'keV')
plt.savefig('h = '+str(COL.h)+'mm, gap = ' + str(DET.gap)[0:4] + 'mm.png',dpi=500)

print('\nRun time = ',time.time()-start_time,'s')