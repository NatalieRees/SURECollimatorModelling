#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 12:29:22 2019

@author: Natalie
"""

import numpy as np
from scipy.interpolate import interp1d
from scipy.integrate import quad

import myclasses as myclass
import myfunctions_circle as myfunc

def radio_model(source,nphot):
    #assign photon energy for a single emission line
    line = float(source)
    ephot = np.tile(line,nphot)
    return ephot

def depletion_depth(ephot,df,density):
    """Return the depletion depth (mm) of the photon as a function of its energy.
    Use log-log interpolation for best result."""
    
    f = interp1d(np.log10(df['keV']),np.log10(df['Tot(cm2/g)']*density))
    u = f(np.log10(ephot))
    u = 10**u
    P_z = np.random.random(len(ephot))
    z = (-1/u)*np.log(P_z)
    return z*10

def integrand(x):
    return np.sin(x)

def assign_angles(iphot,col,det):
    iphot.angle = myclass.Polar()
    theta_max = np.arctan(np.sqrt(det.x**2+det.y**2)/(2*(col.h+col.zdep+det.gap)))
    iphot.angle.theta = np.arccos(1-(1-np.cos(theta_max))*np.random.uniform(0,1,len(iphot.en)))
    iphot.angle.phi = np.random.uniform(0,2*np.pi,len(iphot.en))
    solid_angle =2*np.pi*quad(integrand,0,theta_max)[0]
    return solid_angle/(4*np.pi)

def cartesian_pos(iphot,distance):
    iphot.pos = myclass.Cartesian()
    #x = r sin(theta)cos(phi)
    #y = r sin(theta)sin(phi)
    r = (distance)/np.cos(iphot.angle.theta)
    iphot.pos.x = r*np.sin(iphot.angle.theta)*np.cos(iphot.angle.phi)
    iphot.pos.y = r*np.sin(iphot.angle.theta)*np.sin(iphot.angle.phi)

def pass_thru_radius(iphot,radius):
    distance = np.sqrt(iphot.pos.x**2 + iphot.pos.y**2)
    successful = []
    successful.append(np.where(distance<radius))
    return np.array(successful)[0][0]

def attenuated(iphot,zdep):
    not_attenuated = np.where(iphot.pos.z > zdep)
    return not_attenuated


