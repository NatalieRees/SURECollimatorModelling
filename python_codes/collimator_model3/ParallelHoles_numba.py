# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 10:19:17 2019

@author: nr176
"""


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import time
import matplotlib.patches as mpatches
import os
from numba import jit

import myclasses_step as myclass
import myfunctions_step as myfunc

#define home directory 
homedir = 'Z:/My Documents/python_codes/collimator_model3/'

start_time = time.time()

#Define simulation inputs

#Input parameters for source
CRATE = 1
NFRAME = 1000
NPHOT = CRATE*NFRAME
SOURCE = '122' #line energy in keV

#Create collimator 
COL = myclass.Detector()
COL.x = 24.0 #mm
COL.y = 24.0 #mm
COL.hole_size = 2 #width of square holes or radius of circular holes (mm)
COL.sep = 3 #seperation between centers of adjacent holes (mm)
COL.h = 50 #distance from source to collimator (mm)
COL.zdep = 25 #depth of collimator (mm)
COL.hole_type = 'square' #'square' or 'circle'
COL.array = np.array([7]*7) #shape of array, each element is number of holes in that row
COL.array_type = 'square' #'square' or 'hexagonal'

if COL.array_type == 'square':
    COL.ysep = COL.sep
elif COL.array_type == 'hexagonal':
    COL.ysep = np.sqrt(3)/2*COL.sep #equilateral triangle

center_x = []
for i in COL.array:
    center_x += [list(np.arange(-COL.sep*(i-1)/2,COL.sep*(i+1)/2,COL.sep))]
center_x = np.array(center_x)

y_values = np.arange(-COL.ysep*(len(COL.array)-1)/2,COL.ysep*(len(COL.array)+1)/2,COL.ysep)

#Create detector
DET = myclass.Detector()
DET.x = 20 #mm
DET.y = 20 #mm
DET.pixsize = 0.25 #mm
DET.gap = 20 #Gap between collimator and detector (mm)

#Object and input parameters for material of collimator

#Tungsten
W = myclass.Element()
W.atten = pd.read_csv('W_absorptioncoe_all.csv')
W.Z = 74
W.density = 19.3 #g/cm^3

#Lead
Pb = myclass.Element()
Pb.atten = pd.read_csv('Pb_absorptioncoe_all.csv')
Pb.Z = 82
Pb.density = 11.34 #g/cm^3

atten = Pb.atten
density = Pb.density

#Object to store all incident photon information
iphot = myclass.IncidentPhotons()
iphot.pos = myclass.Cartesian()
iphot.nphot = NPHOT

#Assign energy to each photon
iphot.en = myfunc.radio_model(SOURCE,NPHOT)

#Assign random angles, assuming isotropy
proportion = myfunc.assign_angles(iphot,COL,DET)

#Project photons onto detector
myfunc.cartesian_pos(iphot,COL.h+COL.zdep+DET.gap)

#Finds photons that land outside the detector
inside = np.where((iphot.pos.x<DET.x/2)&(iphot.pos.x>-DET.x/2)&
                      (iphot.pos.y<DET.y/2)&(iphot.pos.y>-DET.y/2))
outside = np.setdiff1d(np.arange(NPHOT),inside)

#Project photons onto surface of collimator
myfunc.cartesian_pos(iphot,COL.h)
iphot.pos.z = np.zeros(iphot.nphot)

iphot.s = np.array([0.0]*NPHOT)

dz = 0.01 #mm

s = []

for i in inside[0]:
    s += [myfunc.step_method_numba(iphot.angle.theta[i],iphot.angle.phi[i],iphot.pos.x[i],iphot.pos.y[i],center_x,y_values,dz,COL.zdep,COL.hole_size,COL.hole_type)]

iphot.s[inside] = s
    
detected = np.setdiff1d(np.where(myfunc.depletion_depth(iphot.en,atten,density)>iphot.s),outside)

print("Counts detected = ",len(detected))
print('Sensitivity = ', len(detected)/NPHOT*proportion)
print('Efficiency = ',len(detected)/len(inside[0])*100,'%')

#Project photons onto detector
myfunc.cartesian_pos(iphot,COL.h+COL.zdep+DET.gap)

#Directory to save results
workdir = 'ParallelHoles_' + SOURCE + 'keV_' + COL.hole_type +'_' + COL.array_type
try:
    os.mkdir(homedir + workdir)
except:
    print('directory already exists...')

os.chdir(homedir + workdir)


#Plots positions of individual photons
f1 = plt.figure()
ax = f1.add_subplot(111)
ax.plot(iphot.pos.x[detected],iphot.pos.y[detected],'o',markersize=0.1)
ax.set_xlim(-COL.x/2,COL.x/2)
ax.set_ylim(-COL.y/2,COL.y/2)
ax.set_xlabel('$x$ (mm)')
ax.set_ylabel('$y$ (mm)')
ax.set_title('h = '+str(COL.h)+'mm, #photons = '+ str(NPHOT) + ', ' + str(SOURCE) +'keV')
#plt.savefig('h = '+str(COL.h)+'mm, #photons = ' + str(NPHOT) + '.png',dpi=500)


#Plots histogram
x = iphot.pos.x[detected]
y = iphot.pos.y[detected]
bins = np.arange(-DET.x/2,DET.x/2+DET.pixsize,DET.pixsize)
f2 = plt.figure()
ax = f2.add_subplot(111)
im = ax.hist2d(x,y,bins=(bins,bins),cmap='coolwarm')
cb  = plt.colorbar(im[3],shrink=0.9,spacing='proportional')
cb.set_label('counts',rotation=270,labelpad=15)
ax.set_xlabel(r'$x$ (mm)')
ax.set_ylabel(r'$y$ (mm)')
ax.set_xlim(-DET.x/2,DET.x/2)
ax.set_ylim(-DET.y/2,DET.y/2)
ax.set_title('h = '+str(COL.h)+'mm, #photons = '+ str(NPHOT) + ', ' + str(SOURCE) +'keV')
plt.savefig('h = '+str(COL.h)+'mm, #photons = ' + str(NPHOT) + ', dz = '+ str(dz) +'.png',dpi=500)
        
print('\nRun time = ',time.time()-start_time,'s')
