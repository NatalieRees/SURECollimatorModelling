#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 13:04:30 2019

@author: Natalie

Simulates the emission of an isotropic point source onto a circular hole collimator 
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import myclasses as myclass
import myfunctions as myfunc

#define home directory 
homedir = '/Users/Natalie/Documents/SURE_2019_student/python_codes/collimator_model3'

#Define simulation inputs

#Input parameters for source
CRATE = 1
NFRAME = 1000000
NPHOT = CRATE*NFRAME
SOURCE = '122' #line energy in keV

#Create collimator 
COL = myclass.Detector()
COL.zdep = int(5.0) #mm
COL.x = int(20.0) #mm
COL.y = int(20.0) #mm
COL.hole = int(2.0) #radius of collimator hole (mm)
    
#Creates a numpy array with values of 0 for hole and 1 for collimator material
block_size = 0.1 #mm
x_blocks = int(COL.x/block_size)
z_blocks = int(COL.zdep/block_size)
shape = np.array([[[1]*x_blocks]*x_blocks]*z_blocks)
for z in np.arange(COL.zdep*10):
    for y in np.arange(-COL.hole*10,COL.hole*10):
        max_x = int(np.sqrt(100*COL.hole**2-y**2))
        for x in np.arange(-max_x,max_x):
            shape[z][x+100][y+100] = 0

#Distance between collimator and source
h = 10.0 #mm

#Create detector
DET = myclass.Detector()
DET.x = COL.x #mm
DET.y = COL.y #mm
DET.pixsize = 0.25 #mm
DET.gap = 20 #Gap between collimator and detector (mm)

#Object and input parameters for material of collimator

#Tungsten
W = myclass.Element()
W.atten = pd.read_csv('W_absorptioncoe_all.csv')
W.Z = 74
W.density = 19.3 #g/cm^3

#Lead
Pb = myclass.Element()
Pb.atten = pd.read_csv('Pb_absorptioncoe_all.csv')
Pb.Z = 82
Pb.density = 11.34 #g/cm^3

atten = W.atten
density = W.density

#Object to store all incident photon information
iphot = myclass.IncidentPhotons()
iphot.pos = myclass.Cartesian()

#Assign energy to each photon
iphot.en = myfunc.radio_model(SOURCE,NPHOT)

#Assign random angles, assuming isotropy
proportion = myfunc.assign_angles(iphot,COL,DET,h)

#Project photons onto surface of collimator
myfunc.cartesian_pos(iphot,h)

iphot.s = np.array([0.0]*NPHOT)

step_size = 0.1 #mm

for i in np.arange(NPHOT):
    ds = step_size/np.cos(iphot.angle.theta[i])
    dx = step_size*np.tan(iphot.angle.theta[i])*np.cos(iphot.angle.phi[i])
    dy = step_size*np.tan(iphot.angle.theta[i])*np.sin(iphot.angle.phi[i])
    while iphot.pos.z[i] < COL.zdep:
        x = int(iphot.pos.x[i]*10+100)
        y = int(iphot.pos.z[i]*10+100)
        z = int(iphot.pos.z[i]*10)
        if shape[z][x][y]==1:
            iphot.s[i]+=ds
            
        iphot.pos.x[i]+=dx
        iphot.pos.y[i]+=dy
        iphot.pos.z[i]+=step_size

#Projects photons onto detector
myfunc.cartesian_pos(iphot,h+COL.dep+DET.gap)
        
#Finds photons that land outside the detector
inside = np.where((iphot.pos.x<DET.x/2)&(iphot.pos.x>-DET.x/2)&
                      (iphot.pos.y<DET.y/2)&(iphot.pos.y>-DET.y/2))
outside = np.setdiff1d(np.arange(NPHOT),inside)

detected = np.setdiff1d(np.where(myfunc.depletion_depth(iphot.en,atten,density)>iphot.s),outside)

print("Counts detected = ",len(detected))
print('Sensitivity = ', len(detected)/NPHOT*proportion)

#Plots positions of individual photons
f1 = plt.figure()
ax = f1.add_subplot(111)
ax.plot(iphot.pos.x[detected],iphot.pos.y[detected],'o',markersize=0.1)
ax.set_xlim(-COL.x/2,COL.x/2)
ax.set_ylim(-COL.y/2,COL.y/2)
ax.set_xlabel('$x$ (mm)')
ax.set_ylabel('$y$ (mm)')
hole = plt.Circle((0,0),COL.hole,color="black",fill=False)
ax.add_patch(hole)
ax.set_title('h = '+str(h))

#Plots histogram
x = iphot.pos.x[detected]
y = iphot.pos.y[detected]
bins = np.arange(-DET.x/2,DET.x/2+DET.pixsize,DET.pixsize)
f2 = plt.figure()
ax = f2.add_subplot(111)
im = ax.hist2d(x,y,bins=(bins,bins),cmap='coolwarm')
cb  = plt.colorbar(im[3],shrink=0.9,spacing='proportional')
cb.set_label('counts',rotation=270,labelpad=15)
hole = plt.Circle((0,0),COL.hole,color="black",fill=False)
ax.add_patch(hole)
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')
ax.set_xlim(-DET.x/2,DET.x/2)
ax.set_ylim(-DET.y/2,DET.y/2)
ax.set_title('h = '+str(h))