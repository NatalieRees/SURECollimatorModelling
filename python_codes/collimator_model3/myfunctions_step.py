#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 12:29:22 2019

@author: Natalie
"""

import numpy as np
from scipy.interpolate import interp1d
import myclasses_step as myclass
from scipy.integrate import quad
from numba import jit

def radio_model(source,nphot):
    #assign photon energy for a single emission line
    line = float(source)
    ephot = np.tile(line,nphot)
    return ephot

def depletion_depth(ephot,df,density):
    """Return the depletion depth (mm) of the photon as a function of its energy.
    Use log-log interpolation for best result."""
    
    f = interp1d(np.log10(df['keV']),np.log10(df['Tot(cm2/g)']*density))
    u = f(np.log10(ephot))
    u = 10**u
    P_z = np.random.random(len(ephot))
    z = (-1/u)*np.log(P_z)
    return z*10

def integrand(x):
    return np.sin(x)

def assign_angles(iphot,col,det):
    iphot.angle = myclass.Polar()
    theta_max = np.arctan(np.sqrt(det.x**2+det.y**2)/(2*(col.h+col.zdep+det.gap)))
    iphot.angle.theta = np.arccos(1-(1-np.cos(theta_max))*np.random.uniform(0,1,len(iphot.en)))
    iphot.angle.phi = np.random.uniform(0,2*np.pi,len(iphot.en))
    solid_angle =2*np.pi*quad(integrand,0,theta_max)[0]
    return solid_angle/(4*np.pi)

def cartesian_pos(iphot,distance):
    #x = r sin(theta)cos(phi)
    #y = r sin(theta)sin(phi)
    r = (distance)/np.cos(iphot.angle.theta)
    iphot.pos.x = r*np.sin(iphot.angle.theta)*np.cos(iphot.angle.phi)
    iphot.pos.y = r*np.sin(iphot.angle.theta)*np.sin(iphot.angle.phi)

def advance(iphot,i,dz,r_crit):
    ds = dz/np.cos(iphot.angle.theta[i])
    dx = dz*np.tan(iphot.angle.theta[i])*np.cos(iphot.angle.phi[i])
    dy = dz*np.tan(iphot.angle.theta[i])*np.sin(iphot.angle.phi[i])
    if iphot.pos.x[i]**2+iphot.pos.y[i]**2 > r_crit**2:
        iphot.s[i]+=ds
    iphot.pos.x[i]+=dx
    iphot.pos.y[i]+=dy
    iphot.pos.z[i]+=dz

def nearest_hole(iphot,col,holes,i):
    distance = np.zeros((col.array,col.array))
    for row in np.arange(0,7):
        for column in np.arange(0,7):
            distance[row][column] = (iphot.pos.x[i]-holes[column])**2+(iphot.pos.y[i]-holes[row])**2
    nearest_row = np.where(distance == np.min(distance))[0][0]
    nearest_column = np.where(distance == np.min(distance))[1][0]
    return nearest_row,nearest_column

#quicker way to find nearest hole
def nearest_hole_square(iphot,col,holes,i):
    base = col.hole+col.septa
    if iphot.pos.x[i] > ((col.array-1)/2*(col.hole+col.septa)):
        nearest_column = col.array-1
    elif iphot.pos.x[i] < (-(col.array-1)/2*(col.hole+col.septa)):
        nearest_column = 0
    else:
        nearest_column = int((col.array-1)/2+round(iphot.pos.x[i]/base))
    
    if iphot.pos.y[i] > ((col.array-1)/2*(col.hole+col.septa)):
        nearest_row = len(holes)-1
    elif iphot.pos.y[i] < (-(col.array-1)/2*(col.hole+col.septa)):
        nearest_row = 0
    else:
        nearest_row =  int((col.array-1)/2+round(iphot.pos.y[i]/base))
    return nearest_row,nearest_column


def inside_hole(iphot,col,center_x,y_values,i):

    nearest_row = (np.abs(y_values-iphot.pos.y[i])).argmin()

    nearest_column = (np.abs(center_x[nearest_row]-iphot.pos.x[i])).argmin()

    if col.hole_type =='square':
        right = center_x[nearest_row][nearest_column]+col.hole_size/2
        left =  center_x[nearest_row][nearest_column]-col.hole_size/2
        top = y_values[nearest_row]+col.hole_size/2
        bottom =  y_values[nearest_row]-col.hole_size/2
        if (iphot.pos.x[i]<right)&(iphot.pos.x[i]>left)&(iphot.pos.y[i]<top)&(iphot.pos.y[i]>bottom):
            inside = 'y'
        else:
            inside = 'n'
    
    if col.hole_type == 'circle':
        distance_squared = (iphot.pos.x[i]-center_x[nearest_row][nearest_column])**2 + (iphot.pos.y[i]-y_values[nearest_row])**2
        if distance_squared < (col.hole_size)**2:
            inside = 'y'
        else:
            inside = 'n'
            
    return inside

def step_method(iphot,col,center_x,y_values,i,dz):
    ds = dz/np.cos(iphot.angle.theta[i])
    dx = dz*np.tan(iphot.angle.theta[i])*np.cos(iphot.angle.phi[i])
    dy = dz*np.tan(iphot.angle.theta[i])*np.sin(iphot.angle.phi[i])
    while iphot.pos.z[i] < col.zdep:
        iphot.pos.x[i]+=dx
        iphot.pos.y[i]+=dy
        iphot.pos.z[i]+=dz
        if inside_hole(iphot,col,center_x,y_values,i) == 'n':
            iphot.s[i]+=ds
    return iphot.s[i]


@jit
def step_method_numba(theta,phi,x,y,center_x,y_values,dz,zdep,hole_size,hole_type):
    z=0.0
    s=0.0
    ds = dz/np.cos(theta)
    dx = dz*np.tan(theta)*np.cos(phi)
    dy = dz*np.tan(theta)*np.sin(phi)
    while z < zdep:
        x+=dx
        y+=dy
        z+=dz
        if inside_hole_numba(x,y,hole_size,hole_type,center_x,y_values) == 'n':
            s+=ds
    return s

@jit
def inside_hole_numba(x,y,hole_size,hole_type,center_x,y_values):
    
    nearest_row = (np.abs(y_values-y)).argmin()

    nearest_column = (np.abs(center_x[nearest_row]-x)).argmin()

    if hole_type =='square':
        right = center_x[nearest_row][nearest_column]+hole_size/2
        left =  center_x[nearest_row][nearest_column]-hole_size/2
        top = y_values[nearest_row]+hole_size/2
        bottom =  y_values[nearest_row]-hole_size/2
        if (x<right)&(x>left)&(y<top)&(y>bottom):
            inside = 'y'
        else:
            inside = 'n'
    
    if hole_type == 'circle':
        distance_squared = (x-center_x[nearest_row][nearest_column])**2 + (y-y_values[nearest_row])**2
        if distance_squared < (hole_size)**2:
            inside = 'y'
        else:
            inside = 'n'
            
    return inside

@jit
def step_method_numba2(theta,phi,x,y,center_x,y_values,dz,zdep,hole_size,hole_type,inside):
    s = []
    for i in inside:
        s += [step_method_numba(theta[i],phi[i],x[i],y[i],center_x,y_values,dz,zdep,hole_size,hole_type)]
    return s