# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 14:04:42 2018

@author: Kjell Koch-Mehrin

Description: Simulation to model a collimator of input design. Simulates the
             trajectory of a chosen number of photons of specified energy/source
             as they pass through a collimator onto a detector. The detector
             is assumed to be perfect and suffers no loss of counting sensitivity
             or energy. Hence, everything that passes through the collimator is 
             observed and recorded. Photons may be attenuated during transmission
             and/or cause flourescence. Attenuation is either via photoelectric
             absoprtion of compton scattering.
             
             Assumptions/Limitations:
                 1. Currently the model only allows square-shaped holes.
                 2. Every photon is incident at the same angle to the collimator
                    surface.
                 3. Photon angles are only incident in one direction i.e. x not y
                 4. All photons incident on the Collimator are ALSO incident on 
                    the detector!!
             

version #1.1
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import os

import myclasses_col as myclass
import myfunctions_col as myfunc

# define home directory
homedir = 'Z:/My Documents/UoL/python_codes/Model/Col_Model/'

#timer
start_time = time.time()

#DEFINE SIMULATION INPUTS

# Input parameters for source
CRATE = 1.0
NFRAME = 100000
SOURCE = '100' # can enter line energy in keV
#ANGLE = np.arange(13) # angle of incidence to the normal of collimator (degrees)
ANGLE = [0]

# Object and input parameter for Collimator
COL = myclass.Detector()
COL.zdep = 25.0 #mm
COL.x = 24 #mm 24 7
COL.y = 24 #mm 24 7
COL.guard = 2.0 #mm 2 1
COL.hole = 2.0 #mm 2 2
COL.septa = 1.0 #mm 1 1

# Object and input parameters for detector under collimator
DETC = myclass.Detector()
DETC.x = 24.0 #mm
DETC.y = 24.0 #mm
DETC.pixsize = 0.5 # mm, 500um

# Define dimensions of the holes and septa
step = COL.hole + COL.septa
hole_left = np.arange(COL.guard, COL.x - COL.guard, step)
hole_right = np.arange(COL.guard + COL.hole, COL.x, step)
holes = np.array((hole_left, hole_right)).transpose()
septa = np.array((holes[0:-1,1], holes[1:,0])).transpose()
guards = np.array(([0,COL.guard],[COL.x - COL.guard, COL.x]))

# Object and input parameters for Material (W) of collimator
W = myclass.Element()
W.atten = pd.read_csv('W_absorptioncoe_all.csv') # from NIST XCOM
W.Z = 74
W.density = 19.3 # g/cm^3
W.u = 183.84 

STATS = myclass.Col_Statistics()

# directory to save results
workdir = SOURCE + 'keV_RSCH'
try:
    os.mkdir(homedir + workdir)
except:
    print('directory already exists...')
    
os.chdir(homedir + workdir)


# run simulation for each input angle
for k in range(len(ANGLE)):
    
    # Object to store all incident photon information
    iphot = myclass.IncidentPhotons()
    
    
    # ***1. Displacement of photons due to incident angle***
    
    # Displacement in x due to incidence angle (mm)
    angle = (ANGLE[k]/360)*2*np.pi #rad
    x_disp = np.tan(angle) * COL.zdep
    
    # Total photon trajectory displacement through collimator
    t_disp = COL.zdep/np.cos(angle)
    
       
    
    # ***2. Incident photon energies and positions***
    
    # Give each photon a position in iphot object
    iphot.en = np.tile(CRATE, NFRAME)
     
    # Randomly generate position for photons on detector surface in x and y (mm)
    iphot.pos.x = np.random.uniform(0, COL.x, len(iphot.en))
    iphot.pos.y = np.random.uniform(0, COL.y, len(iphot.en))
    
    # Randomly give each photon an energy from X-ray source emission probabilities (keV)
    iphot.en = myfunc.radio_model(SOURCE, iphot.en)
    
    # Obtain depletion depth of each photon (mm) i.e. position in z
    iphot.pos.z = myfunc.depletion_depth(iphot.en, W.atten, W.density)
    
       
    
    # ***3. Attenuation through collimator holes, septa and guards***
    
    # Find photons which pass through hole without septa attenuation
    idx_holes = myfunc.pass_thru_holes(iphot, holes, x_disp)
    
    # Find photons which ONLY pass through septa or guards
    idx_septa = myfunc.pass_thru_septa(iphot, septa, guards, x_disp, COL)
    
    # Photons only in septa or guards that are NOT attenuated (probably none)
    idx_esc_septa = np.where(iphot.pos.z[idx_septa] > t_disp)
    idx_esc_septa = idx_septa[idx_esc_septa]
    
    # Use mask to get remaining indexes
    idx = np.concatenate((idx_holes, idx_septa))
    mask = np.zeros(len(iphot.pos.x),dtype=bool)
    mask[idx] = True
    idx_cut = np.where(mask == False)[0]
    
    # Find the distance photons, which partly pass through septa, travel in septa (mm)
    h_septa = myfunc.disp_in_septa(iphot, idx_cut, guards, septa, x_disp, t_disp)
    
    # Photons that are NOT attenuated that are partially in septa/guards
    # this includes photons that cut the very right guard edge and are thus
    # incident to the right of the detector. This may or may not be useful
    # depening on what you are after...
    idx_esc_cut = np.where(iphot.pos.z[idx_cut] > h_septa)
    idx_esc_cut = idx_cut[idx_esc_cut]     
    
    # Calculate the mean displacement of photons that partly pass through septa
    # and the mean of the photons that partly pass through and reach detector
    # this does not include photons that cut the very right guard edge
    # idx_det is used throughout as the index for photons that are actually
    # incident on the detector after passing through collimator
    idx_det = np.where((iphot.pos.x[idx_cut]+x_disp) <= COL.x)
    h_septa = h_septa[idx_det] # h values that are incident/projected on detector
    idx_esc_tmp = np.where(iphot.pos.z[idx_cut][idx_det] > h_septa)
    h_septa_det = h_septa[idx_esc_tmp]
    
    # get the mean displacement of photons that partly pass through septa that
    # were incident on the top of the collimator in a hole
    idx_in_hole = myfunc.incident_in_holes(iphot, holes, idx_cut, idx_det)
    h_septa_in = h_septa[idx_in_hole]
       
             
     
    # ***4. Plots and analysis***
    
    # Remove photons that are not on detector from plot (although still included in stats for ncuts)
    idx_det = np.where((iphot.pos.x[idx_esc_cut]+x_disp) <= COL.x)
    
    # plot all photons that pass through the collimator
    f1 = plt.figure()
    ax = f1.add_subplot(111)
    #plt.plot(iphot.pos.x[idx_holes]+x_disp,iphot.pos.y[idx_holes],'o', markersize=1,label='holes') # where photons end
    #plt.plot(iphot.pos.x[idx_esc_septa]+x_disp,iphot.pos.y[idx_esc_septa],'o', markersize=1,label='septa')
    #plt.plot(iphot.pos.x[idx_esc_cut][idx_det]+x_disp,iphot.pos.y[idx_esc_cut][idx_det],'o', markersize=1,label='cut')
    ax.plot(iphot.pos.x[idx_holes],iphot.pos.y[idx_holes],'o', markersize=1,label='holes') # where photons start
    ax.plot(iphot.pos.x[idx_esc_septa],iphot.pos.y[idx_esc_septa],'o', markersize=1,label='septa')
    ax.plot(iphot.pos.x[idx_esc_cut][idx_det],iphot.pos.y[idx_esc_cut][idx_det],'o', markersize=1,label='cut')
    
    # plot photons that are cutting the edges/guards of the collimator
    idx_off = np.where((iphot.pos.x[idx_esc_cut]+x_disp) > COL.x)
    #plt.plot((COL.x - iphot.pos.x[idx_esc_cut][idx_off]),iphot.pos.y[idx_esc_cut][idx_off],'o', markersize=1,label='cut_edges')
    
    # plot collimator outline
    for i in range(len(holes)):
        ax.vlines(holes,ymin=holes[i][0],ymax=holes[i][1],linewidth=0.5)
        ax.hlines(holes,xmin=holes[i][0],xmax=holes[i][1],linewidth=0.5)
        
    ax.vlines([0,COL.x],ymin=0,ymax=COL.y,linewidth=0.5)
    ax.hlines([0,COL.y],xmin=0,xmax=COL.x,linewidth=0.5)
    
    # label axis
    ax.set_xlabel('$x$ (mm)')
    ax.set_ylabel('$y$ (mm)')
    
    # save figure
    ax.set_title(str(ANGLE[k]) +'degrees')
    plt.savefig(str(ANGLE[k]) +'degrees.png', dpi=500)
    
    
    # create detector image, i.e. histogram
    
    # get all photons which reach detector
    x = np.concatenate((iphot.pos.x[idx_holes],iphot.pos.x[idx_esc_septa],iphot.pos.x[idx_esc_cut]))
    y = np.concatenate((iphot.pos.y[idx_holes],iphot.pos.y[idx_esc_septa],iphot.pos.y[idx_esc_cut]))
    
    # get bins for detector from pixel size and sort
    bins = np.arange(0, DETC.x + DETC.pixsize,DETC.pixsize)
    hist = np.histogram2d(x, y, bins=(bins, bins))
    
    # plot histogram
    f2 = plt.figure()
    ax = f2.add_subplot(111)
    im = ax.imshow(hist[0].T, origin='lower')
    cb = plt.colorbar(im, shrink=0.9)
    cb.set_label('counts', rotation=270, labelpad=15)
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$y$')
    ax.set_title(str(ANGLE[k]) +'degrees')
    
    # plot collimator hole outline
    holes_det = holes/DETC.pixsize - DETC.pixsize
    for i in range(len(holes)):
        ax.vlines(holes_det,ymin=holes_det[i][0],ymax=holes_det[i][1],linewidth=0.5,color='white')
        ax.hlines(holes_det,xmin=holes_det[i][0],xmax=holes_det[i][1],linewidth=0.5, color='white')
    ax.set_xlim(0, DETC.x/DETC.pixsize-1)
    ax.set_ylim(0, DETC.y/DETC.pixsize-1)
    
    # save figure
    plt.savefig(str(ANGLE[k]) +'degrees_det.png',dpi=500)
    
    

    
    # get statistics
    STATS.eff.append(len(idx_holes)/NFRAME)
    STATS.nframe_det.append(NFRAME*((COL.x-x_disp)/COL.x))
    STATS.sens.append((len(idx_holes) + len(idx_esc_septa) + len(idx_esc_cut))/NFRAME)
    STATS.counts.append(len(idx_holes) + len(idx_esc_septa) + len(idx_esc_cut))
    STATS.ncuts.append(len(idx_esc_cut))
    STATS.ncuts_det.append(len(idx_esc_cut[idx_det]))
    STATS.npartialPhots.append(len(h_septa))
    STATS.meanpartialPhotdisp.append(h_septa.mean())
    STATS.meanpartialPhotdisp2Det.append(h_septa_det.mean())
    STATS.meanpartialPhotdispInHole.append(h_septa_in.mean())
    try:
        STATS.minpartialPhotdisp.append(h_septa.min())
    except:
        STATS.minpartialPhotdisp.append(0)
    try:
        STATS.maxpartialPhotdisp.append(h_septa.max())
    except:
        STATS.maxpartialPhotdisp.append(0)
    
    try:
        STATS.minpartialPhotdisp2Det.append(h_septa_det.min())
    except:
        STATS.minpartialPhotdisp2Det.append(0)
    try:
        STATS.maxpartialPhotdisp2Det.append(h_septa_det.max())
    except: 
        STATS.maxpartialPhotdisp2Det.append(0)



# ***5. Write output***
        
# Write CSV
d = vars(STATS)
d['ANGLE'] = ANGLE
df = pd.DataFrame(d)
df.to_csv('output.csv')

# use dir() and vars() on classes
#data = np.array([a,b]).T
#df = pd.DataFrame(data=data,columns=['a','b'])
#data = np.rec.fromarrays([STATS.eff,STATS.sens,STATS.counts],names=['a','b'])
#df = pd.DataFrame(data)

os.chdir('Z:/My Documents/UoL/python_codes/Model/Col_Model')