# -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 11:25:45 2018

@author: Kjell Koch-Mehrin

Description: functions for collimator simulation model
"""

import numpy as np
from scipy.interpolate import interp1d


def radio_model(source, nphot):
    """Randomly give each photon an energy from X-ray source emission 
    probabilities"""
    # **from Table de Radionucleides LNE - LNHB/CEA**
    
    if source == 'Fe':
        # define the emission line energies and their rates (Mg)
        Ka1 = 5.89875
        Ka2 = 5.88765
        Kb3 = 6.49045
        Kb5 = 6.5352
        
        Ka1_rt = 16.57
        Ka2_rt = 8.45
        Kb3_rt = 1.7
        Kb5_rt = 1.7
        rt_tot = sum([Ka1_rt, Ka2_rt, Kb3_rt, Kb5_rt])
        
        # randomly select the energies from rates
        ephot = np.random.choice([Ka1, Ka2, Kb3, Kb5],
                                 len(nphot),
                                 p = [Ka1_rt/rt_tot, Ka2_rt/rt_tot, Kb3_rt/rt_tot,
                                      Kb5_rt/rt_tot])
        
    
    elif source == 'Cd':
        # define the emission line energies and their rates (Ag)
        Ka1 = 22.16317
        Ka2 = 21.9906
        Kb135 = 25.0
        Kb24 = 25.48
        Ll = 2.634
        La = 2.981
        Ln = 2.807
        Lb = 3.151
        Lg = 3.431
        g10 = 88.0336 # gamma emission
        
        Ka1_rt = 29.21
        Ka2_rt = 55.1
        Kb135_rt = 15.25
        Kb24_rt = 2.65
        L_rt = 2.074
        g10_rt = 3.66
        rt_tot = sum([Ka1_rt, Ka2_rt, Kb135_rt, Kb24_rt, L_rt*5, g10_rt])
        
        # randomly select the energies from rates
        ephot = np.random.choice([Ka1, Ka2, Kb135, Kb24, Ll, La, Ln, Lb, Lg, g10],
                                 len(nphot),
                                 p = [Ka1_rt/rt_tot, Ka2_rt/rt_tot, Kb135_rt/rt_tot,
                                      Kb24_rt/rt_tot, L_rt/rt_tot, L_rt/rt_tot, L_rt/rt_tot,
                                      L_rt/rt_tot, L_rt/rt_tot, g10_rt/rt_tot])
        # factor in random spread
        temp = np.where(ephot == Lb)
        ephot[temp] = np.random.uniform(Lb, 3.438, len(temp[0]))
        temp = np.where(ephot == Lg)
        ephot[temp] = np.random.uniform(Lg, 3.748, len(temp[0]))
        
        
    elif source == 'Co':
        # define the emission line energies and their rates (Fe)
        Ka1 = 6.40391
        Ka2 = 6.39091
        Kb1 = 7.05804
        Kb5 = 7.1083
        g10 = 14.41295
        g21 = 122.06065
        g20 = 136.47356
        
        Ka1_rt = 33.5
        Ka2_rt = 17.12
        Kb1_rt = 3.465 
        Kb5_rt = 3.465
        g10_rt = 9.18
        g21_rt = 85.49
        g20_rt = 10.71
        rt_tot = sum([Ka1_rt, Ka2_rt, Kb1_rt, Kb5_rt, g10_rt, g21_rt, g20_rt])
        
        # randomly select the energies from rates
        ephot = np.random.choice([Ka1, Ka2, Kb1, Kb5, g10, g21, g20],
                                 len(nphot),
                                 p = [Ka1_rt/rt_tot, Ka2_rt/rt_tot, Kb1_rt/rt_tot,
                                      Kb5_rt/rt_tot, g10_rt/rt_tot, g21_rt/rt_tot,
                                      g20_rt/rt_tot])
    
    elif source == 'Am':
        # define the emission line energies and their rates (Np)
        # L line emission are from M.C.Lepy 2008 ARI
        Ka1 = 101.059
        Ka2 = 97.069
        Ll = 11.87
        La2 = 13.76
        La1 = 13.95
        Ln = 15.86
        Lb6 = 16.11
        Lb15 = 16.79
        Lb2 = 16.84
        Lb4 = 17.06
        Lb5 = 17.385
        Lb1 = 17.75
        Lb3 = 17.99
        Lg1 = 20.78
        Lg2 = 21.10
        Lg8 = 21.3
        Lg6 = 21.49
        Lg4 = 22.12
        
        g21 = 26.3446
        g11 = 32.183  
        g10 = 33.1963 
        g31 = 42.704
        g42 = 43.420
        g64 = 55.56
        g12 = 57.85
        g20 = 59.5409
        g41 = 69.76
        g62 = 98.97
        g40 = 96.79
        g84 = 123.05
        g61 = 125.30
        
        Ka1_rt = 0.00181
        Ka2_rt = 0.001134
        Ll_rt = 0.837
        La2_rt = 1.398
        La1_rt = 11.60
        Ln_rt = 0.404
        Lb6_rt = 0.248
        Lb15_rt = 0.339
        Lb2_rt = 2.451
        Lb4_rt = 1.736
        Lb5_rt = 0.594
        Lb1_rt = 11.83
        Lb3_rt = 1.310
        Lg1_rt = 2.94
        Lg2_rt = 0.467
        Lg8_rt = 0.520
        Lg6_rt = 0.567
        Lg4_rt = 0.173
        
        g21_rt = 2.31
        g11_rt = 0.0174
        g10_rt = 0.1215
        g31_rt = 0.0055
        g42_rt = 0.0669
        g64_rt = 0.0181
        g12_rt = 0.0052
        g20_rt = 35.92
        g41_rt = 0.0029
        g62_rt = 0.0203
        g40_rt = 0.0195
        g84_rt = 0.0010
        g61_rt = 0.0041
        rates = np.array([Ka1_rt, Ka2_rt, Ll_rt, La2_rt, La1_rt, Ln_rt, Lb6_rt, 
                      Lb15_rt, Lb2_rt, Lb4_rt, Lb5_rt, Lb1_rt, Lb3_rt, Lg1_rt,
                      Lg2_rt, Lg8_rt, Lg6_rt, Lg4_rt, g21_rt, g11_rt, g10_rt,
                      g31_rt, g42_rt, g64_rt, g12_rt, g20_rt, g41_rt, g62_rt,
                      g40_rt, g84_rt, g61_rt])
        rt_tot = sum(rates)
        
        # randomly select the energies from rates
        ephot = np.random.choice([Ka1, Ka2, Ll, La2, La1, Ln, Lb6, Lb15, Lb2,
                                  Lb4, Lb5, Lb1, Lb3, Lg1, Lg2, Lg8, Lg6, Lg4,
                                  g21, g11, g10, g31, g42, g64, g12, g20, g41,
                                  g62, g40, g84, g61],
                                 len(nphot),
                                 p = rates/rt_tot)
        # factor in random spread
        #temp = np.where(ephot == Lb)
        #ephot[temp] = np.random.uniform(Lb, 17.79, len(temp[0]))
        #temp = np.where(ephot == Lg)
        #ephot[temp] = np.random.uniform(Lg, 22.2, len(temp[0]))
        
    else:
        # Single energy emission line
        line = float(source)
        ephot = np.tile(line,len(nphot))
        
    return ephot


def depletion_depth(ephot, df, density):
    """Return the depletion depth (mm) of the photon as a function of its energy.
       Use log-log interpolation for best result."""
    
    f = interp1d(np.log10(df['keV']),np.log10(df['tot(cm2/g)']*density))
    #u = f(E)
    u = f(np.log10(ephot)) # need to use log(x) not x
    u = 10**u # convert log(y) to y
    P_z = np.random.random(len(ephot))
    z = (-1/u)*np.log(P_z) 
    
    return z * 10
    

def cs_or_pe(ephot, df):
    """Return index of photons which were attenuated by compton scattering"""
    
    f1 = interp1d(df['keV'],df['photoelectric effect'])
    f2 = interp1d(df['keV'],df['incoherent'])
    #u = f(E)
    u_pe = f1(ephot)
    u_cs = f2(ephot)
    u_tot = u_pe + u_cs
    P_cs = u_cs/u_tot
    P = np.random.random(len(ephot))
    idx = np.where(P <= P_cs)
    
    return idx


def incident_in_holes(iphot, holes, idx_cut, idx_det):
    """Returns index of photons which are incident in the holes in x and y"""
    
    # Photons with y position within holes
    idx = []
    for i in range(len(holes)):
        idx.append(np.where((iphot.pos.y[idx_cut][idx_det] > holes[i][0]) & (iphot.pos.y[idx_cut][idx_det] < holes[i][1]))[0])
    idx = np.concatenate(idx)
    
    # photons with x positions that stay in holes
    idx_x = []
    for i in range(len(holes)):
        idx_x.append(np.where((iphot.pos.x[idx_cut][idx_det][idx] > holes[i][0]) & (iphot.pos.x[idx_cut][idx_det][idx] < holes[i][1]))[0])
    idx_x = np.concatenate(idx_x)
    
    idx = idx[idx_x]
    
    return idx


def pass_thru_holes(iphot, holes, x_disp):
    """Returns index of photons which pass through the holes clean without any 
       attenuation"""
    
    # Photons with y position within holes
    idx = []
    for i in range(len(holes)):
        idx.append(np.where((iphot.pos.y > holes[i][0]) & (iphot.pos.y < holes[i][1]))[0])
    idx = np.concatenate(idx)
    
    # Maximum position in x after angled flight of photons in y hole
    pos_x_max = iphot.pos.x[idx] + x_disp
    
    # photons with x positions that stay in holes
    idx_x = []
    for i in range(len(holes)):
        idx_x.append(np.where((iphot.pos.x[idx] > holes[i][0]) & (pos_x_max < holes[i][1]))[0])
    idx_x = np.concatenate(idx_x)
    
    idx = idx[idx_x]
    
    return idx


def pass_thru_septa(iphot, septa, guards, x_disp, COL):
    """Returns index of photons which ONLY pass through the septa or guards"""
    
    # Maximum position in x after angled flight of photons in septa
    pos_x_max = iphot.pos.x + x_disp
    
    # Photons which ONLY pass through septa
    idx = []
    for i in range(len(septa)):
        idx.append(np.where(((iphot.pos.y >= septa[i][0]) & (iphot.pos.y <= septa[i][1]) &
                             (pos_x_max <= COL.x)) |
                            ((iphot.pos.x >= septa[i][0]) & (pos_x_max <= septa[i][1]))
                            )[0])
    
    
    #Photons which only pass through guards
    for i in range(len(guards)):
        idx.append(np.where(((iphot.pos.y >= guards[i][0]) & (iphot.pos.y <= guards[i][1]) & 
                             (pos_x_max <= COL.x)) |
                            ((iphot.pos.x >= guards[i][0]) & (pos_x_max <= guards[i][1]))
                            )[0])
    
    idx = np.concatenate(idx)
    idx = np.unique(idx)
    
    return idx

def disp_in_septa(iphot, idx_cut, guards, septa, x_disp, t_disp):
    """Returns the length of displacement (mm) of photons through septa that
       partially pass through septa and/or guards. Sums displacement if photon
       passes through multiple septum and/or septum and guard"""
    
    # initial and final position in x of photon
    x1 = iphot.pos.x[idx_cut]
    x2 = iphot.pos.x[idx_cut] + x_disp
    
    xf = np.zeros(len(x1))
    
    for i in range(len(x1)):
        
        # Consider attenuation by guard bands
        if (x1[i] < guards[0][1]) & (x2[i] > guards[0][1]):
            x = guards[0][1] - x1[i]
            xf[i] = xf[i] + x
            if x2[i] > guards[1][0]:
                x = guards[1][1] - guards[1][0]
                xf[i] = xf[i] + x
                
        elif x1[i] < guards[1][0]:
            if x2[i] >= guards[1][1]:
                x = guards[1][1] - guards[1][0]
                xf[i] = xf[i] + x
            elif (x2[i] > guards[1][0]) & (x2[i] < guards[1][1]):
                x = x2[i] - guards[1][0]
                xf[i] = xf[i] + x
            
        elif (x1[i] >= guards[1][0]) & (x1[i] <= guards[1][1]):
            if x2[i] > guards[1][1]:
                x = guards[1][1] - x1[i]
                xf[i] = xf[i] + x
                
        # Attenuation by septa
        for j in range(len(septa)):
            
            if x1[i] < septa[j][0]:
                if x2[i] >= septa[j][1]:
                    x = septa[j][1] - septa[j][0]
                    xf[i] = xf[i] + x
                elif (x2[i] > septa[j][0]) & (x2[i] < septa[j][1]):
                    x = x2[i] - septa[j][0]
                    xf[i] = xf[i] + x
            
            elif (x1[i] >= septa[j][0]) & (x1[i] < septa[j][1]):
                if x2[i] > septa[j][1]:
                    x = septa[j][1] - x1[i]
                    xf[i] = xf[i] + x
    
    # get the fraction in x-direction that photon is passing through septa/guard                
    xf = xf/x_disp
    
    # get the distance (from hypotenuse) that the photon travels through septa/guard
    h = xf * t_disp
    
    return h