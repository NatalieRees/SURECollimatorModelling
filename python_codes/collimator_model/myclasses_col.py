# -*- coding: utf-8 -*-
"""
Created on Fri Oct 20 09:51:23 2017

@author: Kjell Koch-Mehrin

Description: Classes for Monte Carlo collimator model
"""

import numpy as np

class IncidentPhotons():
    def __init__(self):
        self.pos = Cartesian()
        self.en = []
        self.frame = []
        self.pattern = []
        self.R = []
        self.r = []
    def keep(self, idx):
        self.en = self.en[idx]
        self.frame = self.frame[idx]
        self.pos.x = self.pos.x[idx]
        self.pos.y = self.pos.y[idx]
        self.pos.z = self.pos.z[idx]
    def add(self, new):
        self.en = np.concatenate((self.en, new.en))
        self.frame = np.concatenate((self.frame, new.frame))
        self.pos.x = np.concatenate((self.pos.x, new.pos.x))
        self.pos.y = np.concatenate((self.pos.y, new.pos.y))
        self.pos.z = np.concatenate((self.pos.z, new.pos.z))
    def merge(self):
        self.en = np.concatenate(self.en)
        self.frame = np.concatenate(self.frame)
        self.pos.x = np.concatenate(self.pos.x)
        self.pos.y = np.concatenate(self.pos.y)
        self.pos.z = np.concatenate(self.pos.z)


class Detector():
    def __init__(self):
        self.zdep = 0.72
        self.x = 20.0
        self.y = 20.0
        self.pixsize = 250
        self.guard = 100
    def check_dimensions(self):
        x_check = (self.pixsize/1000)*self.pixarray[0] + 2*(self.guard/1000)
        y_check = (self.pixsize/1000)*self.pixarray[1] + 2*(self.guard/1000)
        if (x_check != self.x) | (y_check != self.y):
            print('ERROR: dimensions of detector and pixels do not match')
            raise SystemExit


class Element():
    def __init__(self):
        self.density = []
        self.atten = []
        self.Z = []


class Cartesian():
    def __init__(self):
        self.x = []
        self.y = []
        self.z = []

        
class Col_Statistics():
    def __init__(self):
        self.eff = []
        self.nframe_det = []
        self.sens = []
        self.counts = []
        self.ncuts = []
        self.ncuts_det = []
        self.npartialPhots = []
        self.meanpartialPhotdisp = []
        self.minpartialPhotdisp = []
        self.maxpartialPhotdisp = []
        self.meanpartialPhotdisp2Det = []
        self.minpartialPhotdisp2Det = []
        self.maxpartialPhotdisp2Det = []
        self.meanpartialPhotdispInHole = []