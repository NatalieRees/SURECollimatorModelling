First calibration file is from the standard specfrom_idl... code where the calibration 
(i.e. the slope and intercept fit) is done in the code.
The second calibration file is from the multi_single analysis code where the calibration
is done using the CAL.fits file from the calibration directory. 

The fact that they are the same shows that using the CAL.fits file is exactly the same.