# -*- coding: utf-8 -*-
"""
Created on Wed Sep  5 16:35:28 2018

Description: read .fits file with pixel spectrum, calibrate each pixel and do
             some analysis. Newer version of specfrom_idlhxttofits_czt.py code,
             which is much more efficient and especially cleaner.

@author: Kjell Koch-Mehrin
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from astropy.io import fits as pyfits

def make_totalspec_fromfits(hdul, hot_count_lim, dims):
    hdul = np.copy(hdul)
    spec = np.zeros(len(hdul[0,0].data)).astype('int64')
    # loop over each pixel spectrum and sum
    for i in range(dims):
        for j in range(dims):
            if hdul[i,j].data.sum() < hot_count_lim:
                spec = spec + hdul[i,j].data
    return spec
    
# pixel calibration for energy spectrum
spec_CAL = True

# CZT 40x40 pixel array switch
CZT = False

# hot pixel threshold
hot_count_lim = 10000
cal_hots = True

# normalise counts
try:
    activity = np.recfromtxt('obs_time_activity.txt')
    obs_time = float(activity[-1][0]) # obervation time
    avg_mbq = float(activity[-1][1]) # average activity
except:
    print('time and average activity of observation not known....')
    obs_time = 1
    avg_mbq = 1

# calibration file locations and names
cal_loc = "\\\\uol.le.ac.uk\\root\\staff\\home\\k\\kalkm1\\My Documents\\UoL\\python_codes\\HEXITEC\\calibration"
cal_dir = '\\uol_cdte\\90adu_bin\\'
#cal_dir = '\\rsch_czt\\detector_B\\100adu_hxt\\'
#cal_dir = '\\ral_cdte\\good\\'
cal_file = 'pix_slope_intercept_sarah.csv'
hot_file = 'hot_pixs_conservative3.csv'

# energy calibration binsize and range
en_binsize = 0.3 # keV
en_binmax = 500 # keV

# adu bin size
adu_bin_size = 10 

# energy window
en_window = [110, 125]

#------------------------------------------------------------------------------
# 1. READ IN FILES
#------------------------------------------------------------------------------

# Filenames of fits files to read
filename_tot = 'totspec_npa_o.fits'
filename_pix = 'pixels_spectra_npa_o.fits'

# Read in fits files
hdul_tot = pyfits.open(filename_tot) 
nbins = len(hdul_tot[0].data)
print('Number of bins:', nbins)

hdul_pix = pyfits.open(filename_pix) 
print('Number of tables for pixel spectra:', len(hdul_pix))

# reshape pixel spectrum list
hdul_pix = np.reshape(hdul_pix,[80,80]) # GigE software always saves output in 80x80 format
if CZT == True:
    hdul_pix = hdul_pix[0::2,1::2] # convert 80x80 pixel array to 40x40 CZT array

# detector dimensions
dims = len(hdul_pix)


#------------------------------------------------------------------------------
# 2. ADU BIN SIZE & GLOBAL CALIBRATION & SPECTRA
#------------------------------------------------------------------------------

# Make adu array
adu = np.arange(0, nbins*adu_bin_size, adu_bin_size)

# summed adu spectrum
f1 = plt.figure()
ax = f1.add_subplot(111)
ax.plot(adu, hdul_tot[0].data,lw=1)
#ax.plot(hdul_tot[0].data)
ax.set_xlabel('adu')
ax.set_ylabel('counts')
#ax.set_yscale('log')
ax.set_title('summed ADU spectrum, all pixels')
plt.savefig('PIX_adu_spectrum.png', dpi=300)
ax.set_yscale('log')
plt.savefig('PIX_adu_spectrum_log.png', dpi=300)

# global energy calibration
#GAIN=0, i.e. high-gain mode, RAL CdTe
#photo_peak_adu = [2130, 4370] # photopeaks are from Fe, Cd, Am, Co spectra
#photo_peak_en = [59.5, 122]
#GAIN=0, i.e. high-gain mode, UoL CdTe
photo_peak_adu = [190, 755, 855, 2080, 3090, 3220, 4230] # photopeaks are from Fe, Cd, Am, Co spectra
photo_peak_en = [5.95, 22, 25, 59.5, 88, 93, 122]
if CZT == True:
    # RSCH detector B
    photo_peak_adu = [1315, 2710, 3486]  # from Tc99m and I-131
    photo_peak_en = [140.5,284.3, 364.5]

# plot, fit and interpolate
z = np.polyfit(photo_peak_adu,photo_peak_en,1) #y=mx+b i.e. polynomial=1
p = np.poly1d(z) # making a 1-d polynomial with slope and intercept in z

# convert adu to energy globally
energy = p(adu)

# summed energy spectrum
f2 = plt.figure()
ax = f2.add_subplot(111)
ax.plot(energy, hdul_tot[0].data,lw=1)
#ax.plot(hdul_tot[0].data)
ax.set_xlabel('energy')
ax.set_ylabel('counts')
#ax.set_yscale('log')
ax.set_title('global energy spectrum, all pixels')
plt.savefig('PIX_globalenergy_spectrum.png', dpi=300)


#------------------------------------------------------------------------------
# 3. DETECTOR IMAGES & COUNTS PER PIXEL HISTOGRAM & NHOT SPECTRUM
#------------------------------------------------------------------------------
pic = np.zeros([dims, dims])
pic_enwin = np.zeros([dims, dims])
hot_pix_map = np.zeros([dims, dims])

# global energy window
idx_enwin = np.where((energy >= en_window[0]) & (energy <= en_window[1]))

# loop through all rows and all columns
for i in range(dims):
    for j in range(dims):
        pic[i,j] = hdul_pix[i,j].data.sum()
        pic_enwin[i,j] = hdul_pix[i,j].data[idx_enwin].sum()

# find hot pixels
hot_pixs = np.where(pic > hot_count_lim)

# detector image no hot pixels
pic_nhot = np.copy(pic)
pic_nhot[hot_pixs] = 0

# remove hot pixels from energy windowed image
pic_enwin[hot_pixs] = 0

# create intermediate hot pixel map
hot_pix_map[hot_pixs] = 1

# create spectrum without intermediate hot pixels
spec_nhots = make_totalspec_fromfits(hdul_pix, hot_count_lim, dims)

# plot counts per pixel histogram
f3 = plt.figure()
ax = f3.add_subplot(111)
ax.hist(pic.flatten(), bins=2000, log=True)
ax.set_xlabel('Counts in pixel')
ax.set_ylabel('Number of pixels')
ax.set_title('Counts in pixel ALL')
plt.savefig('PIX_countsperpixel_histogram.png', dpi=300)

# plot counts per pixel histogram after removing intermeidtae hots
f4 = plt.figure()
ax = f4.add_subplot(111)
ax.hist(pic_nhot.flatten(), bins=100, log=True)
ax.set_xlabel('Counts in pixel')
ax.set_ylabel('Number of pixels')
ax.set_title('Counts in pixel no intermediate hots')
plt.savefig('PIX_countsperpixel_histogram_nointermediate.png', dpi=300)

# summed energy spectrum without intermediate hotpixels
f5 = plt.figure()
ax = f5.add_subplot(111)
ax.plot(energy, spec_nhots,lw=1)
ax.set_xlabel('energy')
ax.set_ylabel('counts')
#ax.set_yscale('log')
ax.set_title('global energy spectrum, no intermediate hot pixels')
plt.savefig('PIX_globalenergy_nhots_spectrum.png', dpi=300)

# plot detector image
f6 = plt.figure()
ax = f6.add_subplot(111)
im = ax.imshow(pic.T, origin='lower')
cb = plt.colorbar(im, shrink=0.9)
cb.set_label('counts', rotation=270, labelpad=15)
ax.set_xlabel(r'$rows$')
ax.set_ylabel(r'$columns$')
ax.set_title('Detector Image: all energies, all pixels')
plt.savefig('PIX_total_image.png', dpi=300)

# plot detector image without hot pixels
f7 = plt.figure()
ax = f7.add_subplot(111)
im = ax.imshow(pic_nhot.T, origin='lower')
cb = plt.colorbar(im, shrink=0.9)
cb.set_label('counts', rotation=270, labelpad=15)
ax.set_xlabel(r'$rows$')
ax.set_ylabel(r'$columns$')
ax.set_title('Detector Image: all energies, no intermediate hot pixels')
plt.savefig('PIX_total_nhot_image.png', dpi=300)

# plot global energy windowed detector image
f8 = plt.figure()
ax = f8.add_subplot(111)
im = ax.imshow(pic_enwin.T, origin='lower',cmap='plasma')
cb = plt.colorbar(im, shrink=0.9)
cb.set_label('counts', rotation=270, labelpad=15)
#ax.set_xlabel(r'$rows$')
#ax.set_ylabel(r'$columns$')
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')
ax.set_title('Detector Image: energy window %s-%s keV, no intermediate hots' % (en_window[0], en_window[1]))
plt.savefig('PIX_%s-%skeV_image.png' % (en_window[0], en_window[1]), dpi=2000)

# plot hot pixel map
f9 = plt.figure()
ax = f9.add_subplot(111)
im = ax.imshow(hot_pix_map.T, cmap='Greys_r', origin='lower')
ax.set_xlabel(r'$rows$')
ax.set_ylabel(r'$columns$')
ax.set_title('Hot pixel map - intermediate')
plt.savefig('PIX_intermediate_hotpixels_image.png', dpi=300)


#------------------------------------------------------------------------------
# 4. CALIBRATED IMAGE & SPECTRUM DATA
#------------------------------------------------------------------------------

# calibrated energy axis
energy_cal = np.arange(0, en_binmax, en_binsize) + en_binsize/2
energy_cal = energy_cal[:-1]

# define calibrated arrays to calculate
pic_enwin_cal = np.zeros([dims, dims])
energy_axis = np.zeros([dims,dims,nbins])
spec_cal = np.zeros(len(energy_cal)) # total energy calibrated spectrum from ALL useable pixels
spec_cal_3D = np.zeros([dims,dims,len(energy_cal)]) # store calibrated and binned spectrum for each pixel for post processing

# select region of pixels
r1 = 20
r2 = 40
c1 = 30
c2 = 50

# select background
bg_r1 = 12
bg_r2 = 30
bg_c1 = 29
bg_c2 = 30

# location of calibration file needed
cal_file = cal_loc + cal_dir + cal_file

# read in calibration file
df = pd.read_csv(cal_file)

# initialize arrays to store slopes, intercepts and r-value
slope = np.zeros([dims,dims])
intercept = np.zeros([dims,dims])
R = np.zeros([dims,dims])

# fill in slope and intercept from calibration file - transpose because calibration file is from bin
for i in range(len(df)):
    slope[int(df.c[i]),int(df.r[i])] = df.slope[i]
    intercept[int(df.c[i]),int(df.r[i])] = df.intercept[i]
    R[int(df.c[i]),int(df.r[i])] = df.R_squared[i]


# get hot pixel file if it exists
hot_file = cal_loc + cal_dir + hot_file

# read in hot pixels file
if cal_hots == True:
    try:
        df = pd.read_csv(hot_file)
        hot_pixs = (np.concatenate((hot_pixs[0], df['r'])),np.concatenate((hot_pixs[1], df['c'])))
    except:
        print('No hot pixel file found...')

# designate hot pixels if R < 0.99
hots_R = np.where(R < 0.90)
hot_pixs = (np.concatenate((hot_pixs[0], hots_R[0])),np.concatenate((hot_pixs[1], hots_R[1])))

# remove spectra in hot pixels
for i in range(len(hot_pixs[0])):
    hdul_pix[hot_pixs[0][i], hot_pixs[1][i]].data = np.zeros(nbins).astype('int64')

# loop though all rows and all columns
for i in range(dims):
    for j in range(dims):
        # get calibrated energy axis of pixel
        energy_axis[i,j] = (adu - intercept[i,j])/slope[i,j]
        # get index of energy window for pixel
        idx_enwin = np.where((energy_axis[i,j] >= en_window[0]) & (energy_axis[i,j] <= en_window[1]))
        # build energy windowed image
        pic_enwin_cal[i,j] = hdul_pix[i,j].data[idx_enwin].sum()
        # build calibrated energy spectrum
        if spec_CAL == True:
            spec_pix = []
            # remove energies with 0 counts
            idx = np.where(hdul_pix[i,j].data > 0)
            counts = hdul_pix[i,j].data[idx]
            CAL_energy = energy_axis[i,j][idx]
            # loop over all remaining energies in pixel
            if len(CAL_energy > 0):
                for k in range(len(CAL_energy)):
                    spec_pix.append([CAL_energy[k]] * counts[k])
                spec_pix = np.concatenate(spec_pix)
                spec_pix = np.histogram(spec_pix, bins = np.arange(0, en_binmax, en_binsize))
                spec_cal = spec_cal + spec_pix[0]
                spec_cal_3D[i,j,:] = spec_pix[0]
        
# create hot pixel map
hot_pix_map[hot_pixs] = 1        

# remove hot pixels
pic_enwin_cal[hot_pixs] = 0

# get selected pixel image
pic_enwin_cal_sel = np.copy(pic_enwin_cal[r1:r2,c1:c2])

# get background
pic_enwin_cal_bkg = np.copy(pic_enwin_cal[bg_r1:bg_r2,bg_c1:bg_c2])

# get average background counts per pixel
bkg = pic_enwin_cal_bkg.mean()/obs_time/avg_mbq

# get total bkg of selected area
bkg_sel = bkg* pic_enwin_cal_sel.size


# plot calibrated energy window
f10 = plt.figure()
ax = f10.add_subplot(111)
im = ax.imshow(pic_enwin_cal.T, origin='lower')
cb = plt.colorbar(im, shrink=0.9)
cb.set_label('counts', rotation=270, labelpad=15)
ax.set_xlabel(r'$rows$')
ax.set_ylabel(r'$columns$')
ax.set_title('Detector Image: calibrated & nhots energy window %s-%s keV' % (en_window[0], en_window[1]))
plt.savefig('PIX_%s-%skeV_CAL_image.png' % (en_window[0], en_window[1]), dpi=300)

# plot calibrated energy window normalized
f11 = plt.figure()
ax = f11.add_subplot(111)
im = ax.imshow(pic_enwin_cal.T/obs_time/avg_mbq, origin='lower')
cb = plt.colorbar(im, shrink=0.9)
cb.set_label('counts/hour/MBq', rotation=270, labelpad=15)
ax.set_xlabel(r'$rows$')
ax.set_ylabel(r'$columns$')
ax.set_title('Detector Image: calibrated & nhots energy window %s-%s keV' % (en_window[0], en_window[1]))
plt.savefig('PIX_%s-%skeV_CAL_normed_image.png' % (en_window[0], en_window[1]), dpi=300)

# plot selected region of pixels
f12 = plt.figure(figsize=(10,10))
ax = f12.add_subplot(111)
im = ax.imshow(pic_enwin_cal_sel.T/obs_time/avg_mbq, origin='lower')
cb = plt.colorbar(im, shrink=0.9)
cb.set_label('counts/hour/MBq', rotation=270, labelpad=15)
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')
#ax.set_xlabel(r'$rows$')
#ax.set_ylabel(r'$columns$')
ax.set_xticklabels(ax.get_xticks().astype(int)+r1)
ax.set_yticklabels(ax.get_yticks().astype(int)+c1)
ax.set_title('Selected Image: calibrated & nhots energy window %s-%s keV' % (en_window[0], en_window[1]))
plt.savefig('PIX_%s-%skeV_CAL_r%s-%s_c%s-%s_normed_image.png' % (en_window[0], en_window[1], r1, r2, c1, c2), dpi=300)

# plot background region of pixels
f13 = plt.figure(figsize=(10,10))
ax = f13.add_subplot(111)
im = ax.imshow(pic_enwin_cal_bkg.T/obs_time/avg_mbq, origin='lower')
cb = plt.colorbar(im, shrink=0.9)
cb.set_label('counts/hour/MBq', rotation=270, labelpad=15)
ax.set_xlabel(r'$rows$')
ax.set_ylabel(r'$columns$')
ax.set_xticklabels(ax.get_xticks().astype(int)+bg_r1)
ax.set_yticklabels(ax.get_yticks().astype(int)+bg_c1)
ax.set_title('Background: calibrated & nhots energy window %s-%s keV' % (en_window[0], en_window[1]))
plt.savefig('PIX_%s-%skeV_CAL_bkg_normed_image.png' % (en_window[0], en_window[1]), dpi=300)

# plot hot pixel map
f14 = plt.figure()
ax = f14.add_subplot(111)
im = ax.imshow(hot_pix_map.T, cmap='Greys_r', origin='lower')
ax.set_xlabel(r'$rows$')
ax.set_ylabel(r'$columns$')
ax.set_title('Hot pixel map - ALL')
plt.savefig('PIX_hotpixels_image_intermediatelim_%s.png' % (hot_count_lim), dpi=300)

 # calibrated energy spectrum without hot pixels
if spec_CAL == True:
    f15 = plt.figure()
    ax = f15.add_subplot(111)
    ax.plot(energy_cal, spec_cal,lw=1)
    ax.set_xlabel('energy')
    ax.set_ylabel('counts')
    ax.set_xlim(0,200)
    ax.set_title('pixel calibrated energy spectrum, no hot pixels')
    plt.savefig('PIX_calibrated_nhots_spectrum.png', dpi=300)
    ax.set_yscale('log')
    plt.savefig('PIX_calibrated_nhots_spectrum_log.png', dpi=300)

#------------------------------------------------------------------------------
# 5. OUTPUT
#------------------------------------------------------------------------------

print('Number of hot pixels:', len(hot_pixs[0]))
print('Total counts nhot pixs, calibrated, & enthres:', pic_enwin_cal.sum())
print('Total counts nhot pixs, calibrated, & enthres, selected:', pic_enwin_cal_sel.sum())
print('Total counts/h/MBq nhot pixs, calibrated, & enthres, selected:', pic_enwin_cal_sel.sum()/obs_time/avg_mbq)
print('bkg counts/h/MBq nhot pixs, calibrated, & enthres per pixel:', bkg)
print('bkg counts/h/MBq nhot pixs, calibrated, & enthres in selected:', bkg_sel)
print('No bkg counts/h/MBq nhot pixs, calibrated, & enthres, selected:', (pic_enwin_cal_sel.sum()/obs_time/avg_mbq)-bkg_sel)

# create dataframe to write out
data = np.array([[nbins], [adu_bin_size], [hot_count_lim], [len(hot_pixs[0])], [en_window[0]],
                 [en_window[1]], [r1], [r2], [c1], [c2], [pic_enwin_cal.sum()], [pic_enwin_cal_sel.sum()],
                 [pic_enwin_cal_sel.sum()/obs_time/avg_mbq], [bkg_sel], [pic_enwin_cal_sel.sum()/obs_time/avg_mbq - bkg_sel]]).T
    
df = pd.DataFrame(data=data,columns=['nbins', 'adu_bin_size', 'hot_count_lim', 'nhot_pixs',
                                     'en_window[0]','en_window[1]', 'r1', 'r2', 'c1', 'c2',
                                     'pic_enwin_cal.sum()', 'pic_enwin_cal_sel.sum()',
                                     'pic_enwin_cal_sel.sum()/h/mbq','bkg_sel',
                                     'pic_enwin_cal_sel.sum()/h/mbq-bkg'])

df.to_csv('PIX_%s-%skeV_CAL_r%s-%s_c%s-%s_summary.csv' % (en_window[0], en_window[1], r1, r2, c1, c2))

# write CALIBRATED spectrum to fits file
if spec_CAL == True:
    #   make data
    tbhdu = pyfits.BinTableHDU.from_columns([
            pyfits.Column(name='ENERGY', format='F', array=energy_cal),
            pyfits.Column(name='COUNTS', format='I64', array=spec_cal)])
    #   make header
    #tbhdu.header  = hdulist_tot[0].header
    tbhdu.header['HOTPIXLIM'] = hot_count_lim
    tbhdu.writeto('PIX_CAL_energy_spectrum.fits', overwrite = True)