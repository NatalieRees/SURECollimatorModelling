# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 11:47:00 2019

@author: nr176
"""

import numpy as np
import myclasses_step as myclass
import myfunctions_stepv2 as myfunc
import sys
import pandas as pd

#Create source
SOURCE = myclass.Detector()
SOURCE.en = '122' #line energy in keV
SOURCE.size = 0  #radius of source (mm)
SOURCE.pos = [[0,0]]

#Create collimator 
COL = myclass.Detector()
COL.x = 20.0 #mm
COL.y = 20.0 #mm
COL.smallhole = 1  #radius of small collimator hole (mm)
COL.bighole = 1.85  #radius of big collimator hole (mm)
COL.alpha = 31*np.pi/360 #Angle at focus of pinhole = 2*alpha
COL.zdep = 2*(COL.bighole-COL.smallhole)/np.tan(COL.alpha) #mm
COL.h = 364.87 #Distance between collimator and source (mm)
COL.material = 'W' #'W' or 'Pb'
COL.hole_type = 'pinhole'
COL.pos = [0,0]
 
#Create detector
DET = myclass.Detector()
DET.x = COL.x #mm
DET.y = COL.y #mm
DET.pixsize = 0.25 #mm
DET.gap = 11.5 #Gap between collimator and detector (mm)

#Tungsten
W = myclass.Element()
W.atten = pd.read_csv('W_absorptioncoe_all.csv')
W.Z = 74
W.density = 19.3 #g/cm^3

#Lead
Pb = myclass.Element()
Pb.atten = pd.read_csv('Pb_absorptioncoe_all.csv')
Pb.Z = 82
Pb.density = 11.34 #g/cm^3

if COL.material == 'W':
    atten = W.atten
    density = W.density
elif COL.material == 'Pb':        
    atten = Pb.atten
    density = Pb.density
else:
    print('Collimator material not recongised')
    sys.exit()

en = myfunc.radio_model(SOURCE.en,1)
mu = myfunc.depletion_depth(en,atten,density)[1][0] #cm^-1

h = COL.h +COL.zdep/2
theta = np.pi/2- np.arctan(np.sqrt(SOURCE.pos[0][0]**2+SOURCE.pos[0][1]**2/h))
d = COL.smallhole*2
alpha = COL.alpha*2

d_eff_sens = np.sqrt(d*(d+2/mu*np.tan(alpha/2))+2/(mu**2)*np.tan(alpha/2)**2) #Paix expression for sensitivity effective diameter

d_eff_res = d+ np.log(2)/mu*np.tan(alpha/2)

g = d_eff_sens**2*np.sin(theta)**3/(16*h**2)

M = (DET.gap+COL.zdep/2)/h

R = d_eff_res*(1+1/M) #resolution in the object 

print('g = ', g)
print('R = ',R)
