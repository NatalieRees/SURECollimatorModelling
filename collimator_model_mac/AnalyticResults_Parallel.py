# -*- coding: utf-8 -*-
"""
Created on Tue Aug  6 15:17:50 2019

@author: nr176
"""

import numpy as np

import myclasses_step as myclass
import myfunctions_stepv2 as myfunc
import sys
import pandas as pd

#Create source
SOURCE = myclass.Detector()
SOURCE.en = '122' #line energy in keV
SOURCE.size = 0 #radius of source (mm)
SOURCE.pos = [[0,0]]

#Create collimator 
COL = myclass.Detector()
COL.x = 44.8 #mm
COL.y = 44.8 #mm
COL.hole_size = 0.3 #width of square holes or radius of circular holes (mm)
COL.sep = 0.35 #seperation between centers of adjacent holes (mm)
COL.h = 50 #distance from source to collimator (mm)
COL.zdep = 20 #depth of collimator (mm)
COL.hole_type = 'square' #'square' or 'circle'
COL.array = np.array([128]*128) #shape of array, each element is number of holes in that row
COL.array_type = 'square' #'square' or 'hexagonal'
COL.material = 'W' #'W' or 'Pb'
COL.pos = [0,0] #position of centre of collimator

#Create detector
DET = myclass.Detector()
DET.x = 44.8 #mm
DET.y = 44.8 #mm
DET.pixsize = 0.35 #mm
DET.gap = 0 #Gap between collimator and detector (mm)

#Tungsten
W = myclass.Element()
W.atten = pd.read_csv('W_absorptioncoe_all.csv')
W.Z = 74
W.density = 19.3 #g/cm^3

#Lead
Pb = myclass.Element()
Pb.atten = pd.read_csv('Pb_absorptioncoe_all.csv')
Pb.Z = 82
Pb.density = 11.34 #g/cm^3

#Titanium
Ti = myclass.Element()
Ti.atten = pd.read_csv('Ti_absorptioncoe_all.csv')
Ti.density = 4.506 #g/cm^3

if COL.material == 'W':
    atten = W.atten
    density = W.density
elif COL.material == 'Pb':        
    atten = Pb.atten
    density = Pb.density
elif COL.material == 'Ti':
    atten = Ti.atten
    density = Ti.density
else:
    print('Collimator material not recongised')
    sys.exit()

if COL.hole_type == 'square':    
    K = 0.28
elif COL.hole_type == 'circle':
    K = 0.24
    
en = myfunc.radio_model(SOURCE.en,1)
mu = myfunc.depletion_depth(en,atten,density)[1][0] #cm^-1
l = COL.zdep/10 #cm
l_eff = l-2/mu #cm

if COL.hole_type == 'circle':
    d = COL.hole_size/5 #cm
elif COL.hole_type == 'square':
    d = COL.hole_size/10 #cm
    
t_check = 6*d/mu/(l-3/mu) #cm
t = (COL.sep-COL.hole_size)/10 #cm

#collimator efficiency
g = K**2*(d/l_eff)**2*d**2/(d+t)**2

#collimator resolution
b = COL.h/10
R = d*(l_eff+b)/l_eff*10 #mm

print('g = ',g)
print('R = ',R)