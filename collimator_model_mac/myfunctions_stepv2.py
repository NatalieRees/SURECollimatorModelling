# -*- coding: utf-8 -*-
"""
Created on Fri Aug 16 15:20:36 2019

@author: nr176
"""


import numpy as np
from scipy.interpolate import interp1d
import myclasses_step as myclass
from scipy.integrate import quad
from numba import jit

def radio_model(source,nphot):
    #assign photon energy for a single emission line
    line = float(source)
    ephot = np.tile(line,nphot)
    return ephot

def depletion_depth(ephot,df,density):
    """Return the depletion depth (mm) of the photon as a function of its energy.
    Use log-log interpolation for best result."""
    
    f = interp1d(np.log10(df['keV']),np.log10(df['Tot(cm2/g)']*density))
    u = f(np.log10(ephot))
    u = 10**u
    P_z = np.random.random(len(ephot))
    z = (-1/u)*np.log(P_z)
    return z*10,u

def integrand(x):
    return np.sin(x)

def assign_angles(iphot,col,det):
    """Assigns random angles to photons isotropically.
    Returns the fractional solid angle subtended"""
    iphot.angle = myclass.Polar()
    theta_max = np.arctan(np.sqrt((det.x/2+np.max(iphot.origin.x))**2+(det.y/2+np.max(iphot.origin.y))**2)/(col.h+col.zdep+det.gap))
    iphot.angle.theta = np.arccos(1-(1-np.cos(theta_max))*np.random.uniform(0,1,len(iphot.en)))
    iphot.angle.phi = np.random.uniform(0,2*np.pi,len(iphot.en))
    solid_angle =2*np.pi*quad(integrand,0,theta_max)[0]
    return solid_angle/(4*np.pi)

def cartesian_pos(iphot,distance):
    """Calculates cartesian positions of photons at given perpendicular distance """
    #x = r sin(theta)cos(phi)
    #y = r sin(theta)sin(phi)
    r = (distance)/np.cos(iphot.angle.theta)
    iphot.pos.x = r*np.sin(iphot.angle.theta)*np.cos(iphot.angle.phi) + iphot.origin.x
    iphot.pos.y = r*np.sin(iphot.angle.theta)*np.sin(iphot.angle.phi) + iphot.origin.y

def full_width_half_max(sigma):
    """FWHM of a gaussian"""
    return 2*np.sqrt(2*np.log(2))*sigma
 
def gaussian(x,mu,sigma):
    return ((1/(np.sqrt(2*np.pi)*sigma))*np.exp(-0.5*(1/sigma*(x-mu))**2))

def twoD_Gaussian(x,amplitude, x0, y0, sigma_x, sigma_y, theta):
    x0 = float(x0)
    y0 = float(y0)
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
    g = amplitude*np.exp( - (a*((x[0]-x0)**2) + 2*b*(x[0]-x0)*(x[1]-y0) + c*((x[1]-y0)**2)))
    return g.ravel()

def solid_angle(a,b,d):
    """Solid angle subtended by a detector with dimension (ab) 
    and source to detector distance d"""
    return 4*np.arctan(a*b/(2*d*np.sqrt(4*d**2+a**2+b**2)))

@jit
def step_method_parallel(theta,phi,x,y,center_x,y_values,dz,zdep,hole_size,hole_type,s_max):
    z=0.0
    s=0.0
    ds = dz/np.cos(theta)
    dx = dz*np.tan(theta)*np.cos(phi)
    dy = dz*np.tan(theta)*np.sin(phi)
    while z < zdep:
        x+=dx
        y+=dy
        z+=dz
        if inside_hole_numba(x,y,hole_size,hole_type,center_x,y_values) == 'n':
            s+=ds
        if s>s_max:
            break
    return s

@jit
def inside_hole_numba(x,y,hole_size,hole_type,center_x,center_y):

    nearest_row = (np.abs(center_y-y)).argmin()

    nearest_column = (np.abs(center_x[nearest_row]-x)).argmin()

    if hole_type == 'square':
        right = center_x[nearest_row][nearest_column]+hole_size/2
        left =  center_x[nearest_row][nearest_column]-hole_size/2
        top = center_y[nearest_row]+hole_size/2
        bottom = center_y[nearest_row]-hole_size/2
        if (x<right)&(x>left)&(y<top)&(y>bottom):
            inside = 'y'
        else:
            inside = 'n'
                    
    if hole_type == 'circle':
        distance_squared = (x-center_x[nearest_row][nearest_column])**2 + (y-center_y[nearest_row])**2
        if distance_squared < (hole_size)**2:
            inside = 'y'
        else:
            inside = 'n'
            
    return inside

@jit
def step_method_pinhole(theta,phi,x,y,zdep,angle,smallhole,x0,y0,dz,s_max):
    z = 0.0
    s = 0.0
    ds = dz/np.cos(theta)
    dx = dz*np.tan(theta)*np.cos(phi)
    dy = dz*np.tan(theta)*np.sin(phi)
    while z < zdep:
        x+=dx
        y+=dy
        z+=dz
        R_crit = abs(zdep/2 - z)*np.tan(angle)+smallhole/2
        if ((x-x0)**2+(y-y0)**2) > (R_crit**2):
            s +=ds
        if s>s_max:
            break
    return s
    