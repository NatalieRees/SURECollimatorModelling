# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 10:31:06 2019

@author: nr176

Simulates the emission of an isotropic point source onto a knife edge pinhole collimator 
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import time
import os
import sys
from scipy.optimize import curve_fit

import myclasses_step as myclass
import myfunctions_stepv2 as myfunc

#define home directory 
homedir = '/Users/Natalie/Documents/SURE_2019_student/collimator_model_mac/'

start_time = time.time()

#Define simulation inputs

#Input parameters for source
CRATE = 1
NFRAME = 1000000
NPHOT = CRATE*NFRAME

#Create source
SOURCE = myclass.Detector()
SOURCE.en = '122' #line energy in keV
SOURCE.size = 0  #radius of source (mm)
SOURCE.pos = [[0,0]]

#Create collimator 
COL = myclass.Detector()
COL.x = 20.0 #mm
COL.y = 20.0 #mm
COL.smallhole = 2  #radius of small collimator hole (mm)
COL.bighole = 2.85  #radius of big collimator hole (mm)
COL.alpha = 31*np.pi/180 #Angle at focus of pinhole 
COL.zdep = 2*(COL.bighole-COL.smallhole)/np.tan(COL.alpha/2) #mm
COL.material = 'W' #'W' or 'Pb'
COL.hole_type = 'pinhole'
COL.pos = [0,0]
COL.h = 50 #Distance between collimator and source (mm) 

#Create detector
DET = myclass.Detector()
DET.x = COL.x #mm
DET.y = COL.y #mm
DET.pixsize = 0.25 #mm
DET.gap = 50 #Gap between collimator and detector (mm)

#step size
dz = 0.005 #mm

#Object and input parameters for material of collimator

#Tungsten
W = myclass.Element()
W.atten = pd.read_csv('W_absorptioncoe_all.csv')
W.Z = 74
W.density = 19.3 #g/cm^3

#Lead
Pb = myclass.Element()
Pb.atten = pd.read_csv('Pb_absorptioncoe_all.csv')
Pb.Z = 82
Pb.density = 11.34 #g/cm^3

if COL.material == 'W':
    atten = W.atten
    density = W.density
elif COL.material == 'Pb':        
    atten = Pb.atten
    density = Pb.density
else:
    print('Collimator material not recongised')
    sys.exit()
    
    
#Object to store all incident photon information
iphot = myclass.IncidentPhotons()
iphot.pos = myclass.Cartesian()
iphot.nphot = NPHOT
iphot.origin = myclass.Cartesian()
iphot.origin.x = np.array([])
iphot.origin.y = np.array([])

#Define shape of sources, currently circle
for source in SOURCE.pos:    
    angle = np.random.uniform(0,2*np.pi,int(NPHOT/len(SOURCE.pos)))
    radius = SOURCE.size*np.sqrt(np.random.uniform(0,1,int(NPHOT/len(SOURCE.pos))))
    iphot.origin.x = np.append(iphot.origin.x,radius*np.cos(angle)+source[0])
    iphot.origin.y = np.append(iphot.origin.y,radius*np.sin(angle)+source[1])


#Assign energy to each photon
iphot.en = myfunc.radio_model(SOURCE.en,NPHOT)

#Assign random angles, assuming isotropy
proportion = myfunc.assign_angles(iphot,COL,DET)

#Project photons onto detector
myfunc.cartesian_pos(iphot,COL.h+COL.zdep+DET.gap)

#Finds photons that land outside the detector
inside = np.where((iphot.pos.x<DET.x/2)&(iphot.pos.x>-DET.x/2)&
                      (iphot.pos.y<DET.y/2)&(iphot.pos.y>-DET.y/2))
outside = np.setdiff1d(np.arange(NPHOT),inside)

#Project photons onto surface of collimator
myfunc.cartesian_pos(iphot,COL.h)
iphot.pos.z = np.zeros(iphot.nphot)

iphot.s = np.array([0.0]*NPHOT)
s_max = myfunc.depletion_depth(iphot.en,atten,density)[0]

s = []

for i in inside[0]:
    s += [myfunc.step_method_pinhole(iphot.angle.theta[i],iphot.angle.phi[i],iphot.pos.x[i],iphot.pos.y[i],COL.zdep,COL.alpha/2,COL.smallhole,COL.pos[0],COL.pos[1],dz,s_max[i])]

iphot.s[inside] = s

detected = np.setdiff1d(np.where(s_max>iphot.s),outside)

print("Counts detected = ",len(detected))
print('Sensitivity = ', len(detected)/NPHOT*proportion)
print('Efficiency = ',len(detected)/len(inside[0])*100,'%')

#Project photons onto detector
myfunc.cartesian_pos(iphot,COL.h+COL.zdep+DET.gap)

#Directory to save results
#workdir = 'Pinhole_' + SOURCE.en + 'keV'
#try:
 #   os.mkdir(homedir + workdir)
#except:
 #   print('directory already exists...')

#os.chdir(homedir + workdir)


#Plots positions of individual photons
#f1 = plt.figure()
#ax = f1.add_subplot(111)
#ax.plot(iphot.pos.x[detected],iphot.pos.y[detected],'o',markersize=0.1)
#ax.set_xlim(-COL.x/2,COL.x/2)
#ax.set_ylim(-COL.y/2,COL.y/2)
#ax.set_xlabel('$x$ (mm)')
#ax.set_ylabel('$y$ (mm)')
#smallhole = plt.Circle((0,0),COL.smallhole,color="black",fill=False)
#ax.add_patch(smallhole)
#bighole = plt.Circle((0,0),COL.bighole,color="black",fill=False)
#ax.add_patch(bighole)
#ax.set_title('h = '+str(COL.h)+'mm, #photons = '+ str(NPHOT) + ', ' + str(SOURCE.en) +'keV')
#plt.savefig('h = '+str(COL.h)+'mm, #photons = ' + str(NPHOT) + '.png',dpi=500)

#Plots histogram
x = iphot.pos.x[detected]
y = iphot.pos.y[detected]
bins = np.arange(-DET.x/2,DET.x/2+DET.pixsize,DET.pixsize)
f2 = plt.figure()
ax = f2.add_subplot(111)
im = ax.hist2d(x,y,bins=(bins,bins),cmap='coolwarm')
cb  = plt.colorbar(im[3],shrink=0.9,spacing='proportional')
cb.set_label('counts',rotation=270,labelpad=15)
smallhole = plt.Circle((0,0),COL.smallhole,color="black",fill=False)
#ax.add_patch(smallhole)
bighole = plt.Circle((0,0),COL.bighole,color="black",fill=False)
#ax.add_patch(bighole)
ax.set_xlabel(r'$x$ (mm)')
ax.set_ylabel(r'$y$ (mm)')
ax.set_xlim(-DET.x/2,DET.x/2)
ax.set_ylim(-DET.y/2,DET.y/2)
ax.set_title('h = '+str(COL.h)+'mm, #photons = '+ str(NPHOT) + ', ' + str(SOURCE.en) +'keV')
#plt.savefig('h = '+str(COL.h)+'mm, #photons = ' + str(NPHOT) + 'gap = '+ str(DET.gap)+'.png',dpi=500)
          
#Magnification
mag = (DET.gap+COL.zdep/2)/(COL.h+COL.zdep/2)

#gaussian fit
mu_x = np.mean(x)
sigma_x = np.std(x)
detector_x = (bins[0:len(bins)-1] + bins[1:len(bins)])/2

mu_y = np.mean(y)
sigma_y = np.std(y)

#2D gaussian fit
init_guess = [np.max(im[0]),mu_x,mu_y,sigma_x,sigma_y,0]
x_values,y_values = np.meshgrid(detector_x,detector_x)
popt,pcov = curve_fit(myfunc.twoD_Gaussian,(x_values,y_values),np.flip(np.transpose(np.flip(im[0],axis=1)),axis=0).ravel(),p0=init_guess)
data_fitted = myfunc.twoD_Gaussian((x_values,y_values),*popt)
fig, ax = plt.subplots()
im2 = ax.imshow(np.flip(data_fitted.reshape(len(detector_x),len(detector_x)),axis=0),cmap='coolwarm')
cb = plt.colorbar(im2, shrink=0.9)
ax.set_title('2D gaussian fit, h = '+str(COL.h)+'mm')
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')
FWHM_gauss_2D = 2*np.sqrt(2*np.log(2))*(popt[3]+popt[4])/(2*mag)

#error in parameters
perr = np.sqrt(np.diag(pcov))
FWHM_error = 2*np.sqrt(2*np.log(2))*(perr[3]+perr[4])/(2*mag)

print('FWHM_gauss_2D = ',FWHM_gauss_2D)
             
print('\nRun time = ',time.time()-start_time,'s')