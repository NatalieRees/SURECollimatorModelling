‹#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 22 13:22:02 2019

@author: Natalie
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import time
import matplotlib.patches as mpatches
import os
import sys
from scipy.optimize import curve_fit

import myclasses_step as myclass
import myfunctions_stepv2 as myfunc

#define home directory 
homedir = '/Users/Natalie/Documents/SURE_2019_student/collimator_model_mac/'

start_time = time.time()
print(time.strftime('%H:%M:%S'))

#------------------------------------------------------------------------------
# 1. SIMULATION INPUTS
#------------------------------------------------------------------------------

#Input parameters for source
CRATE = 1
NFRAME = 1000000
NPHOT = CRATE*NFRAME

#Create source
SOURCE = myclass.Detector()
SOURCE.en = '122' #line energy in keV
SOURCE.size = 0  #radius of source (mm)
SOURCE.pos = [[0,0]] #can include positions of multiple sources eg [[-1,0],[1,0]] (mm)

#Choose collimator type
collimator = 'pinhole' #'pinhole' or 'parallel'

#Define collimator parameters
COL = myclass.Detector()
COL.x = 24.0 #width of collimator in x-direction (mm)
COL.y = 24.0 #width of collimator in y-direction (mm)
COL.material = 'W' #'W' or 'Pb' or 'Ti'
COL.pos = [0,0] #position of the centre of the collimator (mm)
COL.h = 50 #Distance between collimator and source (mm) 

if collimator == 'pinhole':
    COL.small_hole = 2  #diameter of small collimator hole (mm)
    COL.big_hole = 3.7  #diameter of big collimator hole (mm)
    COL.alpha = 31*np.pi/180 #Angle at focus of pinhole 
    COL.zdep = 2*(COL.big_hole/2-COL.small_hole/2)/np.tan(COL.alpha/2) #mm
    
elif collimator == 'parallel':
    COL.hole_size = 2.0 #width of square holes or radius of circular holes (mm)
    COL.sep = 3.0 #seperation between centers of adjacent holes (mm)
    COL.zdep = 35 #depth of collimator (mm)
    COL.hole_type = 'square' #'square' or 'circle'
    COL.array = np.array([7]*7) #shape of array, each element is number of holes in that row
                                #length of COL.array is number of columns
    COL.array_type = 'square' #'square' or 'hexagonal'

else:
    print('Collimator type not supported')
    sys.exit()
    
#Create detector
DET = myclass.Detector()
DET.x = 20 #width of detector in x-direction (mm)
DET.y = 20 #width of detector in y-direction (mm)
DET.pixsize = 0.25 #width of square pixels (mm)
DET.gap = 100 #Gap between collimator and detector (mm)

#step size
dz = 0.01 #mm

#select region of detector of interest by pixel number
selected_region = 'y' #'y' or 'n'

r1 = 20
r2 = 60
c1 = 20
c2 = 60

r1_x = r1*DET.pixsize-DET.x/2
r2_x = r2*DET.pixsize-DET.x/2
c1_y = c1*DET.pixsize-DET.y/2
c2_y = c2*DET.pixsize-DET.y/2

#Object and input parameters for material of collimator

#Tungsten
if COL.material == 'W':
    W = myclass.Element()
    W.atten = pd.read_csv('W_absorptioncoe_all.csv')
    W.Z = 74
    W.density = 19.3 #g/cm^3
    atten = W.atten
    density = W.density
#Lead
elif COL.material == 'Pb': 
    Pb = myclass.Element()
    Pb.atten = pd.read_csv('Pb_absorptioncoe_all.csv')
    Pb.Z = 82
    Pb.density = 11.34 #g/cm^3       
    atten = Pb.atten
    density = Pb.density
#Titanium
elif COL.material == 'Ti':
    Ti = myclass.Element()
    Ti.atten = pd.read_csv('Ti_absorptioncoe_all.csv')
    Ti.density = 4.506 #g/cm^3
    atten = Ti.atten
    density = Ti.density
else:
    print('Collimator material not recongised')
    sys.exit()

#------------------------------------------------------------------------------
# 2. PHOTON INFORMATION
#------------------------------------------------------------------------------
    
#Object to store all incident photon information
iphot = myclass.IncidentPhotons()
iphot.pos = myclass.Cartesian()
iphot.nphot = NPHOT
iphot.origin = myclass.Cartesian()
iphot.origin.x = np.array([])
iphot.origin.y = np.array([])

#Define shape of sources, currently circle
for source in SOURCE.pos:    
    angle = np.random.uniform(0,2*np.pi,int(NPHOT/len(SOURCE.pos)))
    radius = SOURCE.size*np.sqrt(np.random.uniform(0,1,int(NPHOT/len(SOURCE.pos))))
    iphot.origin.x = np.append(iphot.origin.x,radius*np.cos(angle)+source[0])
    iphot.origin.y = np.append(iphot.origin.y,radius*np.sin(angle)+source[1])

#Assign energy to each photon
iphot.en = myfunc.radio_model(SOURCE.en,NPHOT)

#Assign random angles, assuming isotropy
proportion = myfunc.assign_angles(iphot,COL,DET)

#Project photons onto detector
myfunc.cartesian_pos(iphot,COL.h+COL.zdep+DET.gap)

#Finds photons that land outside the detector
inside = np.where((iphot.pos.x<DET.x/2)&(iphot.pos.x>-DET.x/2)&
                      (iphot.pos.y<DET.y/2)&(iphot.pos.y>-DET.y/2))
outside = np.setdiff1d(np.arange(NPHOT),inside)

if selected_region == 'y':
    inside_sel = np.where((iphot.pos.x<r2_x)&(iphot.pos.x>r1_x)&
                         (iphot.pos.y<c2_y)&(iphot.pos.y>c1_y))

    outside_sel = np.setdiff1d(np.arange(NPHOT),inside_sel)

#Project photons onto surface of collimator
myfunc.cartesian_pos(iphot,COL.h)
iphot.pos.z = np.zeros(iphot.nphot)

#Creates attribute to store the path length of photons 
iphot.s = np.array([0.0]*NPHOT)

#Gets max path length for each photon before it is attenuated
iphot.s_max = myfunc.depletion_depth(iphot.en,atten,density)[0]


#------------------------------------------------------------------------------
# 3. STEP METHOD
#------------------------------------------------------------------------------

s = []

if collimator == 'parallel':
    #magnification of collimator
    mag = 1
    if COL.array_type == 'square':
        COL.ysep = COL.sep
    elif COL.array_type == 'hexagonal':
        COL.ysep = np.sqrt(3)/2*COL.sep #equilateral triangle
    else:
        print('Collimator array type not recongised')
        sys.exit()

    center_x = []
    for i in COL.array:
        row = np.arange(-COL.sep*(i-1)/2,COL.sep*(i+1)/2,COL.sep)
        while len(row)<np.max(COL.array): #ensure all rows have the same length so can be used as a numpy array
            row = np.append(row,row[0])
        center_x += [list(row)]
    
    center_x = np.array(center_x)

    y_values = np.arange(-COL.ysep*(len(COL.array)-1)/2,COL.ysep*(len(COL.array)+1)/2,COL.ysep)

    #Shift collimator by shift = (x,y)
    center_x += COL.pos[0]
    y_values += COL.pos[1]
    
    #step method
    for i in inside[0]:
        s += [myfunc.step_method_parallel(iphot.angle.theta[i],iphot.angle.phi[i],iphot.pos.x[i],iphot.pos.y[i],center_x,y_values,dz,COL.zdep,COL.hole_size,COL.hole_type,iphot.s_max[i])]


if collimator == 'pinhole':
    #magnification of collimator
    mag = (DET.gap+COL.zdep/2)/(COL.h+COL.zdep/2)
    #step method
    for i in inside[0]:
        s += [myfunc.step_method_pinhole(iphot.angle.theta[i],iphot.angle.phi[i],iphot.pos.x[i],iphot.pos.y[i],COL.zdep,COL.alpha/2,COL.small_hole,COL.pos[0],COL.pos[1],dz,iphot.s_max[i])]

iphot.s[inside] = s
    
detected = np.setdiff1d(np.where(iphot.s_max>iphot.s),outside)

#------------------------------------------------------------------------------
# 4. DATA PROCESSING AND OUTPUTS
#------------------------------------------------------------------------------

print("Counts detected = ",len(detected))
print('Collimator sensitivity = ', len(detected)/NPHOT*proportion)
print('Collimator efficiency = ',len(detected)/len(inside[0])*100,'%')
if selected_region == 'y':
    detected_sel = np.setdiff1d(detected,outside_sel)
    print('Efficiency of selected region = ',len(detected_sel)/len(inside_sel[0])*100,'%')

#Project photons onto detector
myfunc.cartesian_pos(iphot,COL.h+COL.zdep+DET.gap)

#Directory to save results
if collimator == 'pinhole':    
    workdir = 'Pinhole_' + str(COL.small_hole) +  'mm_'+ SOURCE.en + 'keV_'
if collimator == 'parallel':
    workdir = 'ParallelHole_' + COL.hole_type +'_' + COL.array_type + SOURCE.en + 'keV_'

#try:
 #   os.mkdir(homedir + workdir)
#except:
 #   print('directory already exists...')

#os.chdir(homedir + workdir)


#Plots positions of individual photons
#f1 = plt.figure()
#ax = f1.add_subplot(111)
#ax.plot(iphot.pos.x[detected],iphot.pos.y[detected],'o',markersize=0.1)
#ax.set_xlim(-COL.x/2,COL.x/2)
#ax.set_ylim(-COL.y/2,COL.y/2)
#ax.set_xlabel('$x$ (mm)')
#ax.set_ylabel('$y$ (mm)')
#ax.set_title('h = '+str(COL.h)+'mm, #photons = '+ str(NPHOT) + ', ' + str(SOURCE.en) +'keV')
#plt.savefig('h = '+str(COL.h)+'mm, #photons = ' + str(NPHOT) + '.png',dpi=500)


#Plots histogram
x = iphot.pos.x[detected]
y = iphot.pos.y[detected]
bins = np.arange(-DET.x/2,DET.x/2+DET.pixsize,DET.pixsize)
f2 = plt.figure()
ax = f2.add_subplot(111)
im = ax.hist2d(x,y,bins=(bins,bins),cmap='coolwarm')
cb  = plt.colorbar(im[3],shrink=0.9,spacing='proportional')
cb.set_label('counts',rotation=270,labelpad=15)
ax.set_xlabel(r'$x$ (mm)')
ax.set_ylabel(r'$y$ (mm)')
ax.set_xlim(-DET.x/2,DET.x/2)
ax.set_ylim(-DET.y/2,DET.y/2)
ax.set_title('h = '+str(COL.h)+'mm, #photons = '+ str(NPHOT) + ', ' + str(SOURCE.en) +'keV')
#plt.savefig('h = '+str(COL.h)+'mm, #photons = ' + str(NPHOT) + ', dz = '+ str(dz) +'.png',dpi=500)


#gaussian fit
mu_x = np.mean(x)
sigma_x = np.std(x)
detector_x = (bins[0:len(bins)-1] + bins[1:len(bins)])/2
mu_y = np.mean(y)
sigma_y = np.std(y)

#2D gaussian fit
init_guess = [np.max(im[0]),mu_x,mu_y,sigma_x,sigma_y,0]
x_values,y_values = np.meshgrid(detector_x,detector_x)
popt,pcov = curve_fit(myfunc.twoD_Gaussian,(x_values,y_values),np.flip(np.transpose(np.flip(im[0],axis=1)),axis=0).ravel(),p0=init_guess)

data_fitted = myfunc.twoD_Gaussian((x_values,y_values),*popt)
fig, ax = plt.subplots()
im2 = ax.imshow(np.flip(data_fitted.reshape(len(detector_x),len(detector_x)),axis=0),cmap='coolwarm')
cb = plt.colorbar(im2, shrink=0.9)
ax.set_title('2D gaussian fit, h = '+str(COL.h)+'mm')
ax.set_xlabel(r'$x$ (mm)')
ax.set_ylabel(r'$y$ (mm)')

FWHM_gauss_2D = myfunc.full_width_half_max((popt[3]+popt[4])/2)

#variance in parameters are the diagonal elements of the covariance matrix pcov
perr = np.sqrt(np.diag(pcov))
FWHM_error = myfunc.full_width_half_max((perr[3]+perr[4])/2)

#Resolution in plane of source
res = FWHM_gauss_2D/mag
res_error = FWHM_error/mag

print('FWHM = ',FWHM_gauss_2D,'+/-',FWHM_error,'mm')
print('Resolution = ',res,'+/-',res_error,'mm')

#Plots histogram of selected region
if selected_region =='y':
    x_sel = (iphot.pos.x[detected_sel] +DET.x/2)/DET.pixsize
    y_sel = (iphot.pos.y[detected_sel] +DET.y/2)/DET.pixsize
    bins_x = np.arange(r1,r2+1,1)-0.5
    bins_y = np.arange(c1,c2+1,1)-0.5
    f3 = plt.figure()
    ax = f3.add_subplot(111)
    im = ax.hist2d(x_sel,y_sel,bins=(bins_x,bins_y),cmap='coolwarm')
    cb  = plt.colorbar(im[3],shrink=0.9,spacing='proportional')
    cb.set_label('counts',rotation=270,labelpad=15)
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$y$')
    ax.set_xticklabels(ax.get_xticks().astype(int))
    ax.set_yticklabels(ax.get_yticks().astype(int))
    ax.set_title('h = '+str(COL.h)+'mm, #photons = '+ str(NPHOT) + ', ' + str(SOURCE.en) +'keV')
                 #plt.savefig('h = '+str(COL.h)+'mm, #photons = ' + str(NPHOT)+'_sel.png',dpi=500)

print('\nRun time = ',(time.time()-start_time),'s')
